﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace condition_tool
{
    public partial class top_menu : Form
    {
        public top_menu()
        {
            InitializeComponent();
        }

        private void btn_condition_Click(object sender, EventArgs e)
        {
            condition_main frm = new condition_main();
            this.Visible = false;
            frm.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            account frm = new account();
            this.Visible = false;
            frm.Show();
        }

        private void btn_amz_img_Click(object sender, EventArgs e)
        {
            Amz_img_up frm = new Amz_img_up();
            frm.Show();
            this.Visible = false;
            
        }

        private void top_menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
            this.Close();

            login frm = new login();
            frm.Show();
           
            
        }

        private void btn_yahoo_img_Click(object sender, EventArgs e)
        {
            yahoo_img_up frm = new yahoo_img_up();
            frm.Show();
            this.Visible = false;
        }
    }
}
