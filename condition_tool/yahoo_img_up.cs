﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;
using System.IO; //File.ReadAllLines
//using System.IO.Compression.FileSystem;

namespace condition_tool
{
    public partial class yahoo_img_up : Form
    {
        public yahoo_img_up()
        {

            InitializeComponent();
            // app.exe.configから設定値を取得する。
            tb_img_path.Text = Properties.Settings.Default.drop_box_path;

        }

        private void read()
        {
            string imageDir = this.tb_img_path.Text.ToString(); // 画像ディレクトリ
            string[] jpgFiles =
              System.IO.Directory.GetFiles(imageDir, "*.jpg");

            lv_drop_img.Clear();

            int width = 100;
            int height = 80;

            il_drop_img.ImageSize = new Size(width, height);
            lv_drop_img.LargeImageList = il_drop_img;

            for (int i = 0; i < jpgFiles.Length; i++)
            {
                Image original = Bitmap.FromFile(jpgFiles[i]);
                Image thumbnail = createThumbnail(original, width, height);

                il_drop_img.Images.Add(thumbnail);
                lv_drop_img.Items.Add(jpgFiles[i], i);

                original.Dispose();
                thumbnail.Dispose();
            }
        }

        private void btn_img_read_Click(object sender, EventArgs e)
        {
            string imageDir = this.tb_img_path.Text.ToString(); // 画像ディレクトリ
            string[] jpgFiles = new string[] { };
            jpgFiles = System.IO.Directory.GetFiles(imageDir, "*.jpg");

            lv_drop_img.Clear();
            il_drop_img.Images.Clear();

            int width = 100;
            int height = 80;

            il_drop_img.ImageSize = new Size(width, height);
            lv_drop_img.LargeImageList = il_drop_img;

            for (int i = 0; i < jpgFiles.Length; i++)
            {
                Image original = Bitmap.FromFile(jpgFiles[i]);
                Image thumbnail = createThumbnail(original, width, height);

                il_drop_img.Images.Add(thumbnail);
                lv_drop_img.Items.Add(jpgFiles[i], i);

                original.Dispose();
                thumbnail.Dispose();
            }
        }

        // 幅w、高さhのImageオブジェクトを作成
        Image createThumbnail(Image image, int w, int h)
        {
            Bitmap canvas = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(canvas);
            g.FillRectangle(new SolidBrush(Color.White), 0, 0, w, h);

            float fw = (float)w / (float)image.Width;
            float fh = (float)h / (float)image.Height;

            float scale = Math.Min(fw, fh);
            fw = image.Width * scale;
            fh = image.Height * scale;

            g.DrawImage(image, (w - fw) / 2, (h - fh) / 2, fw, fh);
            g.Dispose();

            return canvas;
        }

        private void btn_csv_read_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                string csvFile = "";
                ofd.Filter = "CSVファイル形式(*.csv)|*.csv";
                DialogResult result = ofd.ShowDialog();

                if (result == DialogResult.OK)
                {
                    csvFile = ofd.FileName;
                    tb_csv_path.Text = csvFile.ToString();
                    if (System.IO.Path.GetExtension(csvFile) != ".csv")
                    {
                        MessageBox.Show("csv形式のファイルを選択してください。");
                        return;
                    }
                    // 選択されたファイル名を格納

                }
                else
                {
                    return;
                }

                TextFieldParser parser = new TextFieldParser(csvFile, Encoding.GetEncoding("Shift_JIS"));
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(","); // 区切り文字はコンマ

                // データをすべてクリア
                dgv_amz_csv.Rows.Clear();
                string[] headerrow = parser.ReadFields();
                for (int i = 0; i < headerrow.Length; i++)
                {
                    // ヘッダー行処理
                }

                string[] lines = File.ReadAllLines(csvFile);

                //Console.WriteLine("{0}", lines.Length);


                dgv_amz_csv.ColumnCount = 4;
                dgv_amz_csv.RowCount = lines.Length - 1;
                int j = 0;
                while (!parser.EndOfData)
                {

                    string[] row = parser.ReadFields(); // 1行読み込み
                                                        // 読み込んだデータ(1行をDataGridViewに表示する)
                    dgv_amz_csv.Rows[j].Cells[0].Value = "";
                    dgv_amz_csv.Rows[j].Cells[1].Value = row[8].Remove(row[8].Length - 7); //末尾の10文字を削除する
                    dgv_amz_csv.Rows[j].Cells[2].Value = row[1];
                    //dgv_amz_csv.Rows[j].Cells[3].Value = row[2];
                    j++;
                }

                check_up();

                tb_img01.Text = "";
                tb_img02.Text = "";
                tb_img03.Text = "";
                tb_img04.Text = "";
                tb_img05.Text = "";
                tb_img06.Text = "";

                this.tb_SKU.Text = "";
                this.tb_title.Text = "";
                //this.tb_condition.Text = "";

                /*
                while (!parser.EndOfData)
                {
                    string[] row = parser.ReadFields(); // 1行読み込み
                                                        // 読み込んだデータ(1行をDataGridViewに表示する)
                    dgv_amz_csv.Rows.Add(row);
                }
                
                for (int i = 0; i < dgv_amz_csv.Rows.Count; i++)
                {

                    string data = dgv_amz_csv.Rows[i].Cells[1].Value.ToString();

                    //if (0 <= data.IndexOf(tb_search.Text))
                    if (0 <= data.IndexOf(tb_search.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;

                    }
                    else
                    {
                        dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                }
                */

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void btn_return_Click(object sender, EventArgs e)
        {
            top_menu frm = new top_menu();
            this.Visible = false;
            frm.Show();
        }

        private void dgv_amz_csv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int gyou_now = 0;


                //現在のセルの行インデックスをがない場合は終了
                if (dgv_amz_csv.CurrentCell == null)
                {
                    return;
                }

                gyou_now = dgv_amz_csv.CurrentCell.RowIndex;

                this.tb_SKU.Text = dgv_amz_csv.Rows[gyou_now].Cells[1].Value.ToString();  //SKU表示
                this.tb_title.Text = dgv_amz_csv.Rows[gyou_now].Cells[2].Value.ToString();  //タイトル表示
                //this.tb_condition.Text = dgv_amz_csv.Rows[gyou_now].Cells[3].Value.ToString();  //コンディション表示
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void btn_upload_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tb_SKU.Text.ToString() == "")
                {
                    // SKUが選択されていません。
                    MessageBox.Show("商品が選択されていません。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    return;
                }
                else
                {
                    int ok_flg = 0;
                    if (this.tb_img01.Text.ToString() == "" || this.tb_img02.Text.ToString() == "" || this.tb_img03.Text.ToString() == "" || this.tb_img04.Text.ToString() == "" || this.tb_img05.Text.ToString() == "" || this.tb_img06.Text.ToString() == "")
                    {
                        //メッセージボックスを表示する
                        DialogResult result = MessageBox.Show("画像が6枚選択されてませんがよろしいですか？5枚以下で問題無い場合はOKボタンを押してください。",
                        "質問",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);

                        if (result == DialogResult.Yes)
                        {
                            ok_flg = 1;
                        }
                        else
                        {
                            return;
                        }

                    }
                    else
                    {
                        ok_flg = 1;
                    }


                    if (ok_flg == 1)
                    {
                        string upfolder = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString();
                        // app.exe.configから設定値を取得する。
                        string ftp_uri = "ftp://" + Properties.Settings.Default.FTP_URI;
                        string ftp_user = Properties.Settings.Default.FTP_USER;
                        string ftp_pw = Properties.Settings.Default.FTP_PW;
                        string ftp_dir = Properties.Settings.Default.FTP_DIR_yahoo;
                        string amz_copy = Properties.Settings.Default.FTP_COPY_yahoo;

                        //アップロードするフォルダ
                        string upFolder = this.tb_SKU.Text.ToString();
                        //アップロード先のURI
                        string uri = ftp_uri + ftp_dir + @"/" + upFolder + @"/";

                        //ファイル一覧を取得するディレクトリのURI
                        Uri u_dir = new Uri(ftp_uri + ftp_dir + @"/");

                        //FtpWebRequestの作成
                        System.Net.FtpWebRequest ftpReq_dir = (System.Net.FtpWebRequest)
                            System.Net.WebRequest.Create(u_dir);
                        //ログインユーザー名とパスワードを設定
                        ftpReq_dir.Credentials = new System.Net.NetworkCredential(ftp_user, ftp_pw);
                        //MethodにWebRequestMethods.Ftp.ListDirectoryDetails("LIST")を設定
                        ftpReq_dir.Method = System.Net.WebRequestMethods.Ftp.ListDirectoryDetails;
                        //要求の完了後に接続を閉じる
                        ftpReq_dir.KeepAlive = false;
                        //PASSIVEモードを無効にする
                        ftpReq_dir.UsePassive = false;

                        //FtpWebResponseを取得
                        System.Net.FtpWebResponse ftpRes_dir =
                            (System.Net.FtpWebResponse)ftpReq_dir.GetResponse();
                        //FTPサーバーから送信されたデータを取得
                        System.IO.StreamReader sr =
                            new System.IO.StreamReader(ftpRes_dir.GetResponseStream());
                        string res = sr.ReadToEnd();
                        //ファイル一覧を表示
                        Console.WriteLine(res);
                        sr.Close();

                        //FTPサーバーから送信されたステータスを表示
                        Console.WriteLine("{0}: {1}", ftpRes_dir.StatusCode, ftpRes_dir.StatusDescription);
                        //閉じる
                        ftpRes_dir.Close();

                        if (0 <= res.IndexOf(upFolder))
                        {

                            MessageBox.Show("既に画像がアップロードされています。アップロードする場合はサーバーから削除してください。",
                                            "エラー",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                        }
                        else
                        {
                            //フォルダを作成する。
                            string foldername = this.tb_SKU.Text.ToString();
                            System.IO.DirectoryInfo di = System.IO.Directory.CreateDirectory(this.tb_img_path.Text.ToString() + @"\" + foldername);


                            //フォルダにファイルを移動する。
                            if (tb_img01.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img01.Text.ToString());

                                //移動後のファイル名
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_01.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img01.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img01.Text.ToString());


                                //画像圧縮END

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                //string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_01.jpg";
                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_01.jpg");

                            }

                            if (tb_img02.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img02.Text.ToString());

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_02.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img02.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img02.Text.ToString());
                                //画像圧縮END


                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_02.jpg");

                            }

                            if (tb_img03.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img03.Text.ToString());

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_03.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img03.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img03.Text.ToString());
                                //画像圧縮END

                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_03.jpg");

                            }

                            if (tb_img04.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img04.Text.ToString());

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_04.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img04.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img04.Text.ToString());
                                //画像圧縮END

                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_04.jpg");

                            }

                            if (tb_img05.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img05.Text.ToString());

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_05.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img05.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img05.Text.ToString());
                                //画像圧縮END

                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_05.jpg");

                            }

                            if (tb_img06.Text.ToString() != "")
                            {
                                //FileInfoオブジェクトを作成する
                                //System.IO.FileInfo fi = new System.IO.FileInfo(tb_img06.Text.ToString());

                                //fiは、移動先のファイルを表すFileInfoに変わる
                                string movename = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_06.jpg";

                                //画像圧縮START
                                Int64 quality = 15; // 品質レベル
                                ImageCodecInfo jpgEncoder = null;

                                // 画像のロード
                                Image myImage = Image.FromFile(tb_img06.Text.ToString());


                                // JPEG用のエンコーダの取得
                                foreach (ImageCodecInfo ici in ImageCodecInfo.GetImageEncoders())
                                {
                                    if (ici.FormatID == ImageFormat.Jpeg.Guid)
                                    {
                                        jpgEncoder = ici;
                                        break;
                                    }
                                }

                                // エンコーダに渡すパラメータの作成
                                EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

                                // パラメータを配列に格納
                                EncoderParameters encParams = new EncoderParameters(1);
                                encParams.Param[0] = encParam;

                                // 画像の保存
                                myImage.Save(movename, jpgEncoder, encParams);
                                myImage.Dispose();

                                //元画像の削除
                                System.IO.File.Delete(tb_img06.Text.ToString());
                                //画像圧縮END

                                //fi.MoveTo(movename);
                            }
                            else
                            {
                                System.IO.File.Copy(@".\nothing.jpg", this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString() + @"\" + tb_SKU.Text.ToString() + "_06.jpg");

                            }

                            //アップロードするファイルをバックアップ
                            string destDirName = amz_copy + @"\" + this.tb_SKU.Text.ToString();
                            string sourceDirName = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString();

                            //コピー先のディレクトリがないときは作る
                            if (!System.IO.Directory.Exists(destDirName))
                            {
                                System.IO.Directory.CreateDirectory(destDirName);
                                //属性もコピー
                                System.IO.File.SetAttributes(destDirName,
                                    System.IO.File.GetAttributes(sourceDirName));
                            }

                            //コピー先のディレクトリ名の末尾に"\"をつける
                            if (destDirName[destDirName.Length - 1] !=
                                    System.IO.Path.DirectorySeparatorChar)
                                destDirName = destDirName + System.IO.Path.DirectorySeparatorChar;

                            //コピー元のディレクトリにあるファイルをコピー
                            string[] files_bk = System.IO.Directory.GetFiles(sourceDirName);
                            foreach (string file in files_bk)
                            {
                                System.IO.File.Copy(file,
                                    destDirName + System.IO.Path.GetFileName(file), true);
                            }

                            Uri u = new Uri(uri);

                            //FtpWebRequestの作成
                            System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)
                                System.Net.WebRequest.Create(u);
                            //ログインユーザー名とパスワードを設定
                            ftpReq.Credentials = new System.Net.NetworkCredential(ftp_user, ftp_pw);
                            //MethodにWebRequestMethods.Ftp.UploadFile("STOR")を設定
                            //ftpReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile;

                            //要求の完了後に接続を閉じる
                            ftpReq.KeepAlive = false;
                            //ASCIIモードで転送する
                            ftpReq.UseBinary = false;
                            //PASVモードを無効にする
                            ftpReq.UsePassive = false;

                            ftpReq.Method = System.Net.WebRequestMethods.Ftp.MakeDirectory;

                            System.Net.FtpWebResponse ftpRes = (System.Net.FtpWebResponse)ftpReq.GetResponse();

                            //FTPサーバーから送信されたステータスを表示
                            Console.WriteLine("{0}: {1}", ftpRes.StatusCode, ftpRes.StatusDescription);
                            ftpRes.Close();

                            //ファイルアップロード
                            //WebClientオブジェクトを作成
                            System.Net.WebClient wc = new System.Net.WebClient();
                            //ログインユーザー名とパスワードを指定
                            wc.Credentials = new System.Net.NetworkCredential(ftp_user, ftp_pw);

                            //FTPサーバーにアップロード

                            //"C:\test"以下の".txt"ファイルをすべて取得する
                            System.IO.DirectoryInfo di2 = new System.IO.DirectoryInfo(this.tb_img_path.Text.ToString() + @"\" + upFolder);
                            System.IO.FileInfo[] files = di.GetFiles("*.jpg", System.IO.SearchOption.AllDirectories);


                            foreach (System.IO.FileInfo f in files)
                            {
                                string Rname = f.FullName.Replace(@"\\", @"\");
                                wc.UploadFile(ftp_uri + ftp_dir + @"/" + upFolder + @"/" + f.ToString(), f.FullName);
                            }
                            //解放する
                            wc.Dispose();

                            MessageBox.Show("アップロード完了しました。",
                                                      "成功",
                                                      MessageBoxButtons.OK,
                                                      MessageBoxIcon.Information);

                            System.IO.Directory.Delete(this.tb_img_path.Text.ToString() + @"\" + upFolder, true);
                        }

                        lv_drop_img.Clear();
                        btn_img_read.PerformClick();
                        btn_img_clear.PerformClick();

                        this.tb_SKU.Text = "";
                        this.tb_title.Text = "";
                        //this.tb_condition.Text = "";

                        check_up();
                    }

                }
            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                btn_img_read.PerformClick();
                btn_img_clear.PerformClick();
                this.tb_SKU.Text = "";
                this.tb_title.Text = "";
                //this.tb_condition.Text = "";
                //this.Close();
            }

        }

        private void lv_drop_img_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            foreach (ListViewItem item in lv_drop_img.SelectedItems)
            {
                //MessageBox.Show(item.Text);
                if (tb_img01.Text.ToString() == "")
                {
                    if (tb_img02.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img03.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img04.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img05.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img06.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img01.Text = item.Text;
                    }
                }
                else if (tb_img02.Text.ToString() == "")
                {
                    if (tb_img01.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img03.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img04.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img05.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img06.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img02.Text = item.Text;
                    }

                }
                else if (tb_img03.Text.ToString() == "")
                {
                    if (tb_img01.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img02.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img04.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img05.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img06.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img03.Text = item.Text;
                    }

                }
                else if (tb_img04.Text.ToString() == "")
                {
                    if (tb_img01.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img02.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img03.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img05.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img06.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img04.Text = item.Text;
                    }

                }
                else if (tb_img05.Text.ToString() == "")
                {
                    if (tb_img01.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img02.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img03.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img04.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img06.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img05.Text = item.Text;
                    }

                }
                else if (tb_img06.Text.ToString() == "")
                {
                    if (tb_img01.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img02.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img03.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img04.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    if (tb_img05.Text.ToString() == item.Text)
                    {
                        // 同じファイルが複数選択。
                        MessageBox.Show("同じファイルが選択されています。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        return;
                    }
                    else
                    {
                        tb_img06.Text = item.Text;
                    }

                }
            }



        }

        private void btn_clear01_Click(object sender, EventArgs e)
        {
            tb_img01.Text = "";
        }

        private void btn_clear02_Click(object sender, EventArgs e)
        {
            tb_img02.Text = "";
        }

        private void btn_clear03_Click(object sender, EventArgs e)
        {
            tb_img03.Text = "";
        }

        private void btn_clear04_Click(object sender, EventArgs e)
        {
            tb_img04.Text = "";
        }

        private void btn_clear05_Click(object sender, EventArgs e)
        {
            tb_img05.Text = "";
        }

        private void btn_clear06_Click(object sender, EventArgs e)
        {
            tb_img06.Text = "";
        }

        private void btn_img_clear_Click(object sender, EventArgs e)
        {
            tb_img01.Text = "";
            tb_img02.Text = "";
            tb_img03.Text = "";
            tb_img04.Text = "";
            tb_img05.Text = "";
            tb_img06.Text = "";
        }

        private void Amz_img_up_Load(object sender, EventArgs e)
        {
            btn_img_read.PerformClick();
            //自分自身のフォームを最大化
            this.WindowState = FormWindowState.Maximized;
        }

        private void Amz_img_up_FormClosing(object sender, FormClosingEventArgs e)
        {
            //メッセージボックスを表示する
            DialogResult result = MessageBox.Show("最終確定をしていますか？最終確定を指定な場合は「いいえ」を、閉じる場合は「はい」を押してください。",
                "質問",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);

            //何が選択されたか調べる
            if (result == DialogResult.Yes)
            {
                //「はい」が選択された時

                top_menu frm = new top_menu();
                this.Visible = false;

                frm.Show();
            }
            else if (result == DialogResult.No)
            {
                //「いいえ」が選択された時
                e.Cancel = true;

            }




        }

        private void img_del_Click(object sender, EventArgs e)
        {
            try
            {
                int idx = 0;
                if (lv_drop_img.SelectedItems.Count > 0)
                {

                    idx = lv_drop_img.SelectedItems[0].Index;

                    //メッセージボックスを表示する
                    DialogResult result = MessageBox.Show("ファイルを削除してよろしいですか？",
                        "質問",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);

                    //何が選択されたか調べる
                    if (result == DialogResult.Yes)
                    {

                        System.IO.File.Delete(lv_drop_img.SelectedItems[0].Text);

                        MessageBox.Show("ファイルを削除しました。",
                                                  "成功",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Information);

                        btn_img_read.PerformClick();

                    }
                    else if (result == DialogResult.No)
                    {
                        //「いいえ」が選択された時
                        return;
                    }
                }
                else
                {
                    return;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dgv_amz_csv.Rows.Count; i++)
                {

                    string data = dgv_amz_csv.Rows[i].Cells[2].Value.ToString();
                    string data2 = dgv_amz_csv.Rows[i].Cells[1].Value.ToString();

                    //if (0 <= data.IndexOf(tb_search.Text))
                    if (0 <= data.IndexOf(tb_search.Text, StringComparison.OrdinalIgnoreCase) || 0 <= data2.IndexOf(tb_search.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        if (dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor != Color.Gray)
                        {
                            dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                        }
                    }
                    else
                    {
                        if (dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor != Color.Gray)
                        {
                            dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void tb_search_KeyDown(object sender, KeyEventArgs e)
        {
            //押されたキーがエンターキーかどうかの条件分岐
            if (e.KeyCode == Keys.Enter)
            {
                btn_search.PerformClick();
            }
        }

        private void btn_dir_open_Click(object sender, EventArgs e)
        {
            try
            {
                //フォルダ"C:\My Documents\My Pictures"を開く
                System.Diagnostics.Process.Start(tb_img_path.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void check_up()
        {
            try
            {
                string upfolder = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString();
                // app.exe.configから設定値を取得する。
                string ftp_uri = "ftp://" + Properties.Settings.Default.FTP_URI;
                string ftp_user = Properties.Settings.Default.FTP_USER;
                string ftp_pw = Properties.Settings.Default.FTP_PW;
                string ftp_dir = Properties.Settings.Default.FTP_DIR_yahoo;

                //アップロードするフォルダ
                string upFolder = this.tb_SKU.Text.ToString();
                //アップロード先のURI
                string uri = ftp_uri + ftp_dir + @"/" + upFolder + @"/";

                //ファイル一覧を取得するディレクトリのURI
                Uri u_dir = new Uri(ftp_uri + ftp_dir + @"/");

                //FtpWebRequestの作成
                System.Net.FtpWebRequest ftpReq_dir = (System.Net.FtpWebRequest)
                    System.Net.WebRequest.Create(u_dir);
                //ログインユーザー名とパスワードを設定
                ftpReq_dir.Credentials = new System.Net.NetworkCredential(ftp_user, ftp_pw);
                //MethodにWebRequestMethods.Ftp.ListDirectoryDetails("LIST")を設定
                ftpReq_dir.Method = System.Net.WebRequestMethods.Ftp.ListDirectoryDetails;
                //要求の完了後に接続を閉じる
                ftpReq_dir.KeepAlive = false;
                //PASSIVEモードを無効にする
                ftpReq_dir.UsePassive = false;

                //FtpWebResponseを取得
                System.Net.FtpWebResponse ftpRes_dir =
                    (System.Net.FtpWebResponse)ftpReq_dir.GetResponse();
                //FTPサーバーから送信されたデータを取得
                System.IO.StreamReader sr =
                    new System.IO.StreamReader(ftpRes_dir.GetResponseStream());
                string res = sr.ReadToEnd();
                //ファイル一覧を表示
                Console.WriteLine(res);
                sr.Close();

                //FTPサーバーから送信されたステータスを表示
                Console.WriteLine("{0}: {1}", ftpRes_dir.StatusCode, ftpRes_dir.StatusDescription);
                //閉じる
                ftpRes_dir.Close();

                for (int i = 0; i < dgv_amz_csv.Rows.Count; i++)
                {

                    string data = dgv_amz_csv.Rows[i].Cells[1].Value.ToString();

                    //if (0 <= data.IndexOf(tb_search.Text))
                    if (0 <= res.IndexOf(dgv_amz_csv.Rows[i].Cells[1].Value.ToString()))
                    {
                        dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                        dgv_amz_csv.Rows[i].Cells[0].Value = "済";

                    }
                    else
                    {
                        //dgv_amz_csv.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        dgv_amz_csv.Rows[i].Cells[0].Value = "未";
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void btn_kako_Click(object sender, EventArgs e)
        {
            // 過去の写真は削除しないでください。
            MessageBox.Show("過去の写真は削除しないでください。",
                            "警告",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

            try
            {
                string amz_copy = Properties.Settings.Default.FTP_COPY_yahoo;
                //フォルダ"C:\My Documents\My Pictures"を開く
                System.Diagnostics.Process.Start(amz_copy);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }

        private void btn_upend_Click(object sender, EventArgs e)
        {
            try
            {
                int ok_flg = 0;
                //メッセージボックスを表示する
                DialogResult result = MessageBox.Show("すべての商品が「済」になっていることを確認してください。",
                "質問",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Yes)
                {
                    ok_flg = 1;
                }
                else
                {
                    return;
                }

                if (ok_flg == 1) {
                    string dir_name = "";
                    dir_name = Path.GetFileNameWithoutExtension(tb_csv_path.Text);
                    string copy_moto = Properties.Settings.Default.FTP_COPY_yahoo;
                    string desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
                    string copy_moto_zip = Properties.Settings.Default.FTP_COPY_yahoo_zip;
                    string zip_path = desktopPath + @"\" + dir_name;

                    //アップロードするファイルをバックアップ

                    //string sourceDirName = this.tb_img_path.Text.ToString() + @"\" + this.tb_SKU.Text.ToString();




                    //ZIP先のディレクトリがないときは作る
                    if (!System.IO.Directory.Exists(zip_path))
                    {
                        System.IO.Directory.CreateDirectory(zip_path);
                        //属性もコピー
                        //System.IO.File.SetAttributes(zip_path,
                        //System.IO.File.GetAttributes(sourceDirName));


                    }



                    //CSVリネーム
                    //FileInfoオブジェクトを作成する
                    System.IO.FileInfo fi = new System.IO.FileInfo(tb_csv_path.Text);
                    // 現在の日付と時刻を取得する
                    DateTime dtNow = DateTime.Now;
                    // 指定した書式で日付を文字列に変換する
                    string stPrompt1 = dtNow.ToString("yyyyMMddHHmmss");

                    string rename = zip_path + @"\" + stPrompt1 + "_yahoo.csv";
                    //fi.MoveTo(rename);
                    //CSVコピー
                    System.IO.File.Copy(tb_csv_path.Text, rename);

                    for (int i = 0; i < dgv_amz_csv.Rows.Count; i++)
                    {


                        string data = dgv_amz_csv.Rows[i].Cells[0].Value.ToString();
                        string kanri_code = dgv_amz_csv.Rows[i].Cells[1].Value.ToString();
                        string copy_path_saki = desktopPath + @"\" + dir_name;
                        string motoDirName = copy_moto + @"\" + kanri_code;

                        if (data == "済")
                        {
                            //コピー先のディレクトリ名の末尾に"\"をつける
                            if (copy_path_saki[copy_path_saki.Length - 1] !=
                                    System.IO.Path.DirectorySeparatorChar)
                                copy_path_saki = copy_path_saki + System.IO.Path.DirectorySeparatorChar;

                            //コピー元のディレクトリにあるファイルをコピー
                            string[] files_bk = System.IO.Directory.GetFiles(motoDirName);
                            foreach (string file in files_bk)
                            {
                                System.IO.File.Copy(file,
                                    copy_path_saki + System.IO.Path.GetFileName(file), true);
                            }

                        }
                        else
                        {

                        }
                    }

                    //作成するZIP書庫のパス
                    //ファイルが既に存在している場合は、上書きされる
                    string zipFileName = zip_path + @".zip";
                    //圧縮するフォルダのパス
                    string sourceDirectory = zip_path;


                    //ZIP書庫を作成
                    System.IO.Compression.ZipFile.CreateFromDirectory(
                        sourceDirectory,
                        zipFileName,
                        System.IO.Compression.CompressionLevel.Optimal,
                        false,
                        System.Text.Encoding.GetEncoding("shift_jis"));

                    //フォルダ"C:\TEST"を削除する
                    //第2項をTrueにすると、"C:\TEST"を根こそぎ（サブフォルダ、ファイルも）削除する
                    //"C:\TEST"に読み取り専用ファイルがあると、UnauthorizedAccessExceptionが発生
                    //"C:\TEST"が存在しないと、DirectoryNotFoundExceptionが発生
                    System.IO.Directory.Delete(sourceDirectory, true);

                    MessageBox.Show("デスクトップにヤフオク出品用zipファイルが書き出されました。\nChromeでオークタウンを開いて、出品してください。",
                                                      "成功",
                                                      MessageBoxButtons.OK,
                                                      MessageBoxIcon.Information);

                    System.IO.File.Copy(zipFileName, copy_moto_zip + @"\" + System.IO.Path.GetFileName(zipFileName), true);
                    //System.IO.File.Delete(tb_csv_path.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                //this.Close();
            }
        }
    }
}