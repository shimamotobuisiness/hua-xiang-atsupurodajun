﻿namespace condition_tool
{
    partial class Amz_img_up
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lv_drop_img = new System.Windows.Forms.ListView();
            this.cm_list = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.img_del = new System.Windows.Forms.ToolStripMenuItem();
            this.il_drop_img = new System.Windows.Forms.ImageList(this.components);
            this.btn_img_read = new System.Windows.Forms.Button();
            this.btn_csv_read = new System.Windows.Forms.Button();
            this.dgv_amz_csv = new System.Windows.Forms.DataGridView();
            this.check = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condhition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_return = new System.Windows.Forms.Button();
            this.tb_SKU = new System.Windows.Forms.TextBox();
            this.tb_title = new System.Windows.Forms.TextBox();
            this.tb_img01 = new System.Windows.Forms.TextBox();
            this.tb_img02 = new System.Windows.Forms.TextBox();
            this.tb_img03 = new System.Windows.Forms.TextBox();
            this.tb_img04 = new System.Windows.Forms.TextBox();
            this.tb_img05 = new System.Windows.Forms.TextBox();
            this.tb_img06 = new System.Windows.Forms.TextBox();
            this.lb_SKU = new System.Windows.Forms.Label();
            this.lb_img01 = new System.Windows.Forms.Label();
            this.lb_img02 = new System.Windows.Forms.Label();
            this.lb_img03 = new System.Windows.Forms.Label();
            this.lb_img04 = new System.Windows.Forms.Label();
            this.lb_img05 = new System.Windows.Forms.Label();
            this.lb_img06 = new System.Windows.Forms.Label();
            this.btn_img_clear = new System.Windows.Forms.Button();
            this.btn_upload = new System.Windows.Forms.Button();
            this.btn_clear01 = new System.Windows.Forms.Button();
            this.btn_clear02 = new System.Windows.Forms.Button();
            this.btn_clear03 = new System.Windows.Forms.Button();
            this.btn_clear04 = new System.Windows.Forms.Button();
            this.btn_clear05 = new System.Windows.Forms.Button();
            this.btn_clear06 = new System.Windows.Forms.Button();
            this.tb_img_path = new System.Windows.Forms.TextBox();
            this.tb_condition = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_search = new System.Windows.Forms.Button();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.btn_dir_open = new System.Windows.Forms.Button();
            this.btn_kako = new System.Windows.Forms.Button();
            this.cm_list.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_amz_csv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_drop_img
            // 
            this.lv_drop_img.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lv_drop_img.ContextMenuStrip = this.cm_list;
            this.lv_drop_img.LargeImageList = this.il_drop_img;
            this.lv_drop_img.Location = new System.Drawing.Point(729, 120);
            this.lv_drop_img.MultiSelect = false;
            this.lv_drop_img.Name = "lv_drop_img";
            this.lv_drop_img.Size = new System.Drawing.Size(382, 305);
            this.lv_drop_img.TabIndex = 0;
            this.lv_drop_img.UseCompatibleStateImageBehavior = false;
            this.lv_drop_img.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_drop_img_MouseDoubleClick);
            // 
            // cm_list
            // 
            this.cm_list.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.img_del});
            this.cm_list.Name = "cm_list";
            this.cm_list.Size = new System.Drawing.Size(101, 26);
            // 
            // img_del
            // 
            this.img_del.Name = "img_del";
            this.img_del.Size = new System.Drawing.Size(100, 22);
            this.img_del.Text = "削除";
            this.img_del.Click += new System.EventHandler(this.img_del_Click);
            // 
            // il_drop_img
            // 
            this.il_drop_img.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.il_drop_img.ImageSize = new System.Drawing.Size(16, 16);
            this.il_drop_img.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btn_img_read
            // 
            this.btn_img_read.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_img_read.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_img_read.Location = new System.Drawing.Point(821, 40);
            this.btn_img_read.Name = "btn_img_read";
            this.btn_img_read.Size = new System.Drawing.Size(161, 50);
            this.btn_img_read.TabIndex = 1;
            this.btn_img_read.Text = "Dropbox内の画像\r\n最新状態に再読込";
            this.btn_img_read.UseVisualStyleBackColor = true;
            this.btn_img_read.Click += new System.EventHandler(this.btn_img_read_Click);
            // 
            // btn_csv_read
            // 
            this.btn_csv_read.Location = new System.Drawing.Point(12, 11);
            this.btn_csv_read.Name = "btn_csv_read";
            this.btn_csv_read.Size = new System.Drawing.Size(103, 44);
            this.btn_csv_read.TabIndex = 3;
            this.btn_csv_read.Text = "商品リストCSV\r\nファイル読み込み";
            this.btn_csv_read.UseVisualStyleBackColor = true;
            this.btn_csv_read.Click += new System.EventHandler(this.btn_csv_read_Click);
            // 
            // dgv_amz_csv
            // 
            this.dgv_amz_csv.AllowUserToAddRows = false;
            this.dgv_amz_csv.AllowUserToDeleteRows = false;
            this.dgv_amz_csv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_amz_csv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_amz_csv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_amz_csv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.check,
            this.SKU,
            this.title,
            this.condhition});
            this.dgv_amz_csv.Location = new System.Drawing.Point(12, 120);
            this.dgv_amz_csv.MultiSelect = false;
            this.dgv_amz_csv.Name = "dgv_amz_csv";
            this.dgv_amz_csv.ReadOnly = true;
            this.dgv_amz_csv.RowHeadersVisible = false;
            this.dgv_amz_csv.RowTemplate.Height = 21;
            this.dgv_amz_csv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_amz_csv.Size = new System.Drawing.Size(711, 307);
            this.dgv_amz_csv.TabIndex = 4;
            this.dgv_amz_csv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_amz_csv_CellDoubleClick);
            // 
            // check
            // 
            this.check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.check.DefaultCellStyle = dataGridViewCellStyle2;
            this.check.FillWeight = 5F;
            this.check.HeaderText = "済";
            this.check.Name = "check";
            this.check.ReadOnly = true;
            // 
            // SKU
            // 
            this.SKU.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SKU.FillWeight = 20F;
            this.SKU.HeaderText = "SKU";
            this.SKU.Name = "SKU";
            this.SKU.ReadOnly = true;
            // 
            // title
            // 
            this.title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.title.FillWeight = 80F;
            this.title.HeaderText = "タイトル";
            this.title.Name = "title";
            this.title.ReadOnly = true;
            // 
            // condhition
            // 
            this.condhition.HeaderText = "コンディション";
            this.condhition.Name = "condhition";
            this.condhition.ReadOnly = true;
            this.condhition.Width = 89;
            // 
            // btn_return
            // 
            this.btn_return.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_return.Location = new System.Drawing.Point(821, 11);
            this.btn_return.Name = "btn_return";
            this.btn_return.Size = new System.Drawing.Size(290, 23);
            this.btn_return.TabIndex = 5;
            this.btn_return.Text = "トップメニューへ";
            this.btn_return.UseVisualStyleBackColor = true;
            this.btn_return.Click += new System.EventHandler(this.btn_return_Click);
            // 
            // tb_SKU
            // 
            this.tb_SKU.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_SKU.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb_SKU.Location = new System.Drawing.Point(72, 42);
            this.tb_SKU.Name = "tb_SKU";
            this.tb_SKU.ReadOnly = true;
            this.tb_SKU.Size = new System.Drawing.Size(405, 19);
            this.tb_SKU.TabIndex = 6;
            // 
            // tb_title
            // 
            this.tb_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_title.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_title.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb_title.Location = new System.Drawing.Point(73, 10);
            this.tb_title.Name = "tb_title";
            this.tb_title.ReadOnly = true;
            this.tb_title.Size = new System.Drawing.Size(602, 26);
            this.tb_title.TabIndex = 7;
            // 
            // tb_img01
            // 
            this.tb_img01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img01.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img01.Location = new System.Drawing.Point(61, 437);
            this.tb_img01.Name = "tb_img01";
            this.tb_img01.ReadOnly = true;
            this.tb_img01.Size = new System.Drawing.Size(377, 19);
            this.tb_img01.TabIndex = 8;
            // 
            // tb_img02
            // 
            this.tb_img02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img02.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img02.Location = new System.Drawing.Point(557, 437);
            this.tb_img02.Name = "tb_img02";
            this.tb_img02.ReadOnly = true;
            this.tb_img02.Size = new System.Drawing.Size(377, 19);
            this.tb_img02.TabIndex = 9;
            // 
            // tb_img03
            // 
            this.tb_img03.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img03.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img03.Location = new System.Drawing.Point(61, 462);
            this.tb_img03.Name = "tb_img03";
            this.tb_img03.ReadOnly = true;
            this.tb_img03.Size = new System.Drawing.Size(377, 19);
            this.tb_img03.TabIndex = 10;
            // 
            // tb_img04
            // 
            this.tb_img04.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img04.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img04.Location = new System.Drawing.Point(557, 461);
            this.tb_img04.Name = "tb_img04";
            this.tb_img04.ReadOnly = true;
            this.tb_img04.Size = new System.Drawing.Size(377, 19);
            this.tb_img04.TabIndex = 11;
            // 
            // tb_img05
            // 
            this.tb_img05.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img05.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img05.Location = new System.Drawing.Point(61, 488);
            this.tb_img05.Name = "tb_img05";
            this.tb_img05.ReadOnly = true;
            this.tb_img05.Size = new System.Drawing.Size(377, 19);
            this.tb_img05.TabIndex = 12;
            // 
            // tb_img06
            // 
            this.tb_img06.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_img06.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_img06.Location = new System.Drawing.Point(557, 489);
            this.tb_img06.Name = "tb_img06";
            this.tb_img06.ReadOnly = true;
            this.tb_img06.Size = new System.Drawing.Size(377, 19);
            this.tb_img06.TabIndex = 13;
            // 
            // lb_SKU
            // 
            this.lb_SKU.AutoSize = true;
            this.lb_SKU.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lb_SKU.Location = new System.Drawing.Point(15, 24);
            this.lb_SKU.Name = "lb_SKU";
            this.lb_SKU.Size = new System.Drawing.Size(52, 30);
            this.lb_SKU.TabIndex = 14;
            this.lb_SKU.Text = "選択中\r\n商品";
            this.lb_SKU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_img01
            // 
            this.lb_img01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img01.AutoSize = true;
            this.lb_img01.Location = new System.Drawing.Point(14, 440);
            this.lb_img01.Name = "lb_img01";
            this.lb_img01.Size = new System.Drawing.Size(41, 12);
            this.lb_img01.TabIndex = 16;
            this.lb_img01.Text = "画像01";
            // 
            // lb_img02
            // 
            this.lb_img02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img02.AutoSize = true;
            this.lb_img02.Location = new System.Drawing.Point(510, 442);
            this.lb_img02.Name = "lb_img02";
            this.lb_img02.Size = new System.Drawing.Size(41, 12);
            this.lb_img02.TabIndex = 17;
            this.lb_img02.Text = "画像02";
            // 
            // lb_img03
            // 
            this.lb_img03.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img03.AutoSize = true;
            this.lb_img03.Location = new System.Drawing.Point(14, 466);
            this.lb_img03.Name = "lb_img03";
            this.lb_img03.Size = new System.Drawing.Size(41, 12);
            this.lb_img03.TabIndex = 18;
            this.lb_img03.Text = "画像03";
            // 
            // lb_img04
            // 
            this.lb_img04.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img04.AutoSize = true;
            this.lb_img04.Location = new System.Drawing.Point(510, 468);
            this.lb_img04.Name = "lb_img04";
            this.lb_img04.Size = new System.Drawing.Size(41, 12);
            this.lb_img04.TabIndex = 19;
            this.lb_img04.Text = "画像04";
            // 
            // lb_img05
            // 
            this.lb_img05.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img05.AutoSize = true;
            this.lb_img05.Location = new System.Drawing.Point(14, 492);
            this.lb_img05.Name = "lb_img05";
            this.lb_img05.Size = new System.Drawing.Size(41, 12);
            this.lb_img05.TabIndex = 20;
            this.lb_img05.Text = "画像05";
            // 
            // lb_img06
            // 
            this.lb_img06.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_img06.AutoSize = true;
            this.lb_img06.Location = new System.Drawing.Point(510, 494);
            this.lb_img06.Name = "lb_img06";
            this.lb_img06.Size = new System.Drawing.Size(41, 12);
            this.lb_img06.TabIndex = 21;
            this.lb_img06.Text = "画像06";
            // 
            // btn_img_clear
            // 
            this.btn_img_clear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_img_clear.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_img_clear.Location = new System.Drawing.Point(988, 491);
            this.btn_img_clear.Name = "btn_img_clear";
            this.btn_img_clear.Size = new System.Drawing.Size(127, 23);
            this.btn_img_clear.TabIndex = 22;
            this.btn_img_clear.Text = "選択画像をすべて解除";
            this.btn_img_clear.UseVisualStyleBackColor = true;
            this.btn_img_clear.Click += new System.EventHandler(this.btn_img_clear_Click);
            // 
            // btn_upload
            // 
            this.btn_upload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_upload.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_upload.Location = new System.Drawing.Point(988, 435);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(127, 45);
            this.btn_upload.TabIndex = 23;
            this.btn_upload.Text = "画像確定";
            this.btn_upload.UseVisualStyleBackColor = true;
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            // 
            // btn_clear01
            // 
            this.btn_clear01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear01.Location = new System.Drawing.Point(443, 435);
            this.btn_clear01.Name = "btn_clear01";
            this.btn_clear01.Size = new System.Drawing.Size(42, 23);
            this.btn_clear01.TabIndex = 24;
            this.btn_clear01.Text = "解除";
            this.btn_clear01.UseVisualStyleBackColor = true;
            this.btn_clear01.Click += new System.EventHandler(this.btn_clear01_Click);
            // 
            // btn_clear02
            // 
            this.btn_clear02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear02.Location = new System.Drawing.Point(940, 431);
            this.btn_clear02.Name = "btn_clear02";
            this.btn_clear02.Size = new System.Drawing.Size(42, 23);
            this.btn_clear02.TabIndex = 25;
            this.btn_clear02.Text = "解除";
            this.btn_clear02.UseVisualStyleBackColor = true;
            this.btn_clear02.Click += new System.EventHandler(this.btn_clear02_Click);
            // 
            // btn_clear03
            // 
            this.btn_clear03.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear03.Location = new System.Drawing.Point(443, 462);
            this.btn_clear03.Name = "btn_clear03";
            this.btn_clear03.Size = new System.Drawing.Size(42, 23);
            this.btn_clear03.TabIndex = 26;
            this.btn_clear03.Text = "解除";
            this.btn_clear03.UseVisualStyleBackColor = true;
            this.btn_clear03.Click += new System.EventHandler(this.btn_clear03_Click);
            // 
            // btn_clear04
            // 
            this.btn_clear04.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear04.Location = new System.Drawing.Point(940, 461);
            this.btn_clear04.Name = "btn_clear04";
            this.btn_clear04.Size = new System.Drawing.Size(42, 23);
            this.btn_clear04.TabIndex = 27;
            this.btn_clear04.Text = "解除";
            this.btn_clear04.UseVisualStyleBackColor = true;
            this.btn_clear04.Click += new System.EventHandler(this.btn_clear04_Click);
            // 
            // btn_clear05
            // 
            this.btn_clear05.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear05.Location = new System.Drawing.Point(443, 489);
            this.btn_clear05.Name = "btn_clear05";
            this.btn_clear05.Size = new System.Drawing.Size(42, 23);
            this.btn_clear05.TabIndex = 28;
            this.btn_clear05.Text = "解除";
            this.btn_clear05.UseVisualStyleBackColor = true;
            this.btn_clear05.Click += new System.EventHandler(this.btn_clear05_Click);
            // 
            // btn_clear06
            // 
            this.btn_clear06.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_clear06.Location = new System.Drawing.Point(940, 490);
            this.btn_clear06.Name = "btn_clear06";
            this.btn_clear06.Size = new System.Drawing.Size(42, 23);
            this.btn_clear06.TabIndex = 29;
            this.btn_clear06.Text = "解除";
            this.btn_clear06.UseVisualStyleBackColor = true;
            this.btn_clear06.Click += new System.EventHandler(this.btn_clear06_Click);
            // 
            // tb_img_path
            // 
            this.tb_img_path.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_img_path.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb_img_path.Location = new System.Drawing.Point(821, 96);
            this.tb_img_path.Name = "tb_img_path";
            this.tb_img_path.Size = new System.Drawing.Size(290, 18);
            this.tb_img_path.TabIndex = 30;
            // 
            // tb_condition
            // 
            this.tb_condition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_condition.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_condition.Location = new System.Drawing.Point(483, 42);
            this.tb_condition.Name = "tb_condition";
            this.tb_condition.ReadOnly = true;
            this.tb_condition.Size = new System.Drawing.Size(192, 19);
            this.tb_condition.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Controls.Add(this.lb_SKU);
            this.panel1.Controls.Add(this.tb_condition);
            this.panel1.Controls.Add(this.tb_SKU);
            this.panel1.Controls.Add(this.tb_title);
            this.panel1.Location = new System.Drawing.Point(122, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(693, 79);
            this.panel1.TabIndex = 33;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(648, 91);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 35;
            this.btn_search.Text = "商品検索";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // tb_search
            // 
            this.tb_search.Location = new System.Drawing.Point(389, 95);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(253, 19);
            this.tb_search.TabIndex = 34;
            this.tb_search.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_search_KeyDown);
            // 
            // btn_dir_open
            // 
            this.btn_dir_open.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_dir_open.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btn_dir_open.Location = new System.Drawing.Point(988, 40);
            this.btn_dir_open.Name = "btn_dir_open";
            this.btn_dir_open.Size = new System.Drawing.Size(123, 50);
            this.btn_dir_open.TabIndex = 36;
            this.btn_dir_open.Text = "Dropbox\r\nフォルダを開く";
            this.btn_dir_open.UseVisualStyleBackColor = true;
            this.btn_dir_open.Click += new System.EventHandler(this.btn_dir_open_Click);
            // 
            // btn_kako
            // 
            this.btn_kako.Location = new System.Drawing.Point(12, 61);
            this.btn_kako.Name = "btn_kako";
            this.btn_kako.Size = new System.Drawing.Size(103, 44);
            this.btn_kako.TabIndex = 38;
            this.btn_kako.Text = "過去写真の確認";
            this.btn_kako.UseVisualStyleBackColor = true;
            this.btn_kako.Click += new System.EventHandler(this.btn_kako_Click);
            // 
            // Amz_img_up
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 521);
            this.Controls.Add(this.btn_kako);
            this.Controls.Add(this.btn_dir_open);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.tb_img_path);
            this.Controls.Add(this.btn_clear06);
            this.Controls.Add(this.btn_clear05);
            this.Controls.Add(this.btn_clear04);
            this.Controls.Add(this.btn_clear03);
            this.Controls.Add(this.btn_clear02);
            this.Controls.Add(this.btn_clear01);
            this.Controls.Add(this.btn_upload);
            this.Controls.Add(this.btn_img_clear);
            this.Controls.Add(this.lb_img06);
            this.Controls.Add(this.lb_img05);
            this.Controls.Add(this.lb_img04);
            this.Controls.Add(this.lb_img03);
            this.Controls.Add(this.lb_img02);
            this.Controls.Add(this.lb_img01);
            this.Controls.Add(this.tb_img06);
            this.Controls.Add(this.tb_img05);
            this.Controls.Add(this.tb_img04);
            this.Controls.Add(this.tb_img03);
            this.Controls.Add(this.tb_img02);
            this.Controls.Add(this.tb_img01);
            this.Controls.Add(this.btn_return);
            this.Controls.Add(this.dgv_amz_csv);
            this.Controls.Add(this.btn_csv_read);
            this.Controls.Add(this.btn_img_read);
            this.Controls.Add(this.lv_drop_img);
            this.Controls.Add(this.panel1);
            this.Name = "Amz_img_up";
            this.Text = "Amazon画像アップロード";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Amz_img_up_FormClosing);
            this.Load += new System.EventHandler(this.Amz_img_up_Load);
            this.cm_list.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_amz_csv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_drop_img;
        private System.Windows.Forms.ImageList il_drop_img;
        private System.Windows.Forms.Button btn_img_read;
        private System.Windows.Forms.Button btn_csv_read;
        private System.Windows.Forms.DataGridView dgv_amz_csv;
        private System.Windows.Forms.Button btn_return;
        private System.Windows.Forms.TextBox tb_SKU;
        private System.Windows.Forms.TextBox tb_title;
        private System.Windows.Forms.TextBox tb_img01;
        private System.Windows.Forms.TextBox tb_img02;
        private System.Windows.Forms.TextBox tb_img03;
        private System.Windows.Forms.TextBox tb_img04;
        private System.Windows.Forms.TextBox tb_img05;
        private System.Windows.Forms.TextBox tb_img06;
        private System.Windows.Forms.Label lb_SKU;
        private System.Windows.Forms.Label lb_img01;
        private System.Windows.Forms.Label lb_img02;
        private System.Windows.Forms.Label lb_img03;
        private System.Windows.Forms.Label lb_img04;
        private System.Windows.Forms.Label lb_img05;
        private System.Windows.Forms.Label lb_img06;
        private System.Windows.Forms.Button btn_img_clear;
        private System.Windows.Forms.Button btn_upload;
        private System.Windows.Forms.Button btn_clear01;
        private System.Windows.Forms.Button btn_clear02;
        private System.Windows.Forms.Button btn_clear03;
        private System.Windows.Forms.Button btn_clear04;
        private System.Windows.Forms.Button btn_clear05;
        private System.Windows.Forms.Button btn_clear06;
        private System.Windows.Forms.TextBox tb_img_path;
        private System.Windows.Forms.TextBox tb_condition;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip cm_list;
        private System.Windows.Forms.ToolStripMenuItem img_del;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Button btn_dir_open;
        private System.Windows.Forms.DataGridViewTextBoxColumn check;
        private System.Windows.Forms.DataGridViewTextBoxColumn SKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn condhition;
        private System.Windows.Forms.Button btn_kako;
    }
}