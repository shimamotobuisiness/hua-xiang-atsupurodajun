﻿namespace condition_tool
{
    partial class account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgb_account = new System.Windows.Forms.DataGridView();
            this.lb_accountid = new System.Windows.Forms.Label();
            this.tb_s_accountid = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.lb_accountid2 = new System.Windows.Forms.Label();
            this.tb_accountid = new System.Windows.Forms.TextBox();
            this.lb_pass = new System.Windows.Forms.Label();
            this.tb_pass = new System.Windows.Forms.TextBox();
            this.cb_kengen1 = new System.Windows.Forms.CheckBox();
            this.cb_kengen2 = new System.Windows.Forms.CheckBox();
            this.bt_return = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgb_account)).BeginInit();
            this.SuspendLayout();
            // 
            // dgb_account
            // 
            this.dgb_account.AllowUserToAddRows = false;
            this.dgb_account.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgb_account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgb_account.Location = new System.Drawing.Point(12, 82);
            this.dgb_account.MultiSelect = false;
            this.dgb_account.Name = "dgb_account";
            this.dgb_account.ReadOnly = true;
            this.dgb_account.RowTemplate.Height = 21;
            this.dgb_account.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgb_account.Size = new System.Drawing.Size(491, 154);
            this.dgb_account.TabIndex = 0;
            this.dgb_account.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgb_account_CellClick);
            // 
            // lb_accountid
            // 
            this.lb_accountid.AutoSize = true;
            this.lb_accountid.Location = new System.Drawing.Point(12, 24);
            this.lb_accountid.Name = "lb_accountid";
            this.lb_accountid.Size = new System.Drawing.Size(60, 12);
            this.lb_accountid.TabIndex = 1;
            this.lb_accountid.Text = "アカウントID";
            // 
            // tb_s_accountid
            // 
            this.tb_s_accountid.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tb_s_accountid.Location = new System.Drawing.Point(87, 21);
            this.tb_s_accountid.Name = "tb_s_accountid";
            this.tb_s_accountid.Size = new System.Drawing.Size(146, 19);
            this.tb_s_accountid.TabIndex = 2;
            this.tb_s_accountid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_s_accountid_KeyPress);
            // 
            // btn_search
            // 
            this.btn_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_search.Location = new System.Drawing.Point(428, 17);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 3;
            this.btn_search.Text = "検索";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // btn_new
            // 
            this.btn_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_new.Location = new System.Drawing.Point(14, 344);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(75, 23);
            this.btn_new.TabIndex = 4;
            this.btn_new.Text = "新規";
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // btn_update
            // 
            this.btn_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_update.Location = new System.Drawing.Point(95, 344);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(75, 23);
            this.btn_update.TabIndex = 5;
            this.btn_update.Text = "登録・変更";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_delete.Location = new System.Drawing.Point(218, 344);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 6;
            this.btn_delete.Text = "削除";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // lb_accountid2
            // 
            this.lb_accountid2.AutoSize = true;
            this.lb_accountid2.Location = new System.Drawing.Point(12, 255);
            this.lb_accountid2.Name = "lb_accountid2";
            this.lb_accountid2.Size = new System.Drawing.Size(60, 12);
            this.lb_accountid2.TabIndex = 7;
            this.lb_accountid2.Text = "アカウントID";
            // 
            // tb_accountid
            // 
            this.tb_accountid.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tb_accountid.Location = new System.Drawing.Point(78, 252);
            this.tb_accountid.Name = "tb_accountid";
            this.tb_accountid.Size = new System.Drawing.Size(100, 19);
            this.tb_accountid.TabIndex = 8;
            this.tb_accountid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_accountid_KeyPress);
            // 
            // lb_pass
            // 
            this.lb_pass.AutoSize = true;
            this.lb_pass.Location = new System.Drawing.Point(216, 255);
            this.lb_pass.Name = "lb_pass";
            this.lb_pass.Size = new System.Drawing.Size(52, 12);
            this.lb_pass.TabIndex = 9;
            this.lb_pass.Text = "パスワード";
            // 
            // tb_pass
            // 
            this.tb_pass.Location = new System.Drawing.Point(274, 252);
            this.tb_pass.Name = "tb_pass";
            this.tb_pass.PasswordChar = '*';
            this.tb_pass.Size = new System.Drawing.Size(100, 19);
            this.tb_pass.TabIndex = 10;
            this.tb_pass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pass_KeyPress);
            // 
            // cb_kengen1
            // 
            this.cb_kengen1.AutoSize = true;
            this.cb_kengen1.Location = new System.Drawing.Point(14, 288);
            this.cb_kengen1.Name = "cb_kengen1";
            this.cb_kengen1.Size = new System.Drawing.Size(157, 16);
            this.cb_kengen1.TabIndex = 12;
            this.cb_kengen1.Text = "コンディション設定マスタ権限";
            this.cb_kengen1.UseVisualStyleBackColor = true;
            this.cb_kengen1.CheckedChanged += new System.EventHandler(this.cb_kengen1_CheckedChanged);
            // 
            // cb_kengen2
            // 
            this.cb_kengen2.AutoSize = true;
            this.cb_kengen2.Location = new System.Drawing.Point(14, 310);
            this.cb_kengen2.Name = "cb_kengen2";
            this.cb_kengen2.Size = new System.Drawing.Size(118, 16);
            this.cb_kengen2.TabIndex = 13;
            this.cb_kengen2.Text = "アカウントマスタ権限";
            this.cb_kengen2.UseVisualStyleBackColor = true;
            this.cb_kengen2.CheckedChanged += new System.EventHandler(this.cb_kengen2_CheckedChanged);
            // 
            // bt_return
            // 
            this.bt_return.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_return.Location = new System.Drawing.Point(428, 344);
            this.bt_return.Name = "bt_return";
            this.bt_return.Size = new System.Drawing.Size(75, 23);
            this.bt_return.TabIndex = 14;
            this.bt_return.Text = "戻る";
            this.bt_return.UseVisualStyleBackColor = true;
            this.bt_return.Click += new System.EventHandler(this.bt_return_Click);
            // 
            // account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 379);
            this.Controls.Add(this.bt_return);
            this.Controls.Add(this.cb_kengen2);
            this.Controls.Add(this.cb_kengen1);
            this.Controls.Add(this.tb_pass);
            this.Controls.Add(this.lb_pass);
            this.Controls.Add(this.tb_accountid);
            this.Controls.Add(this.lb_accountid2);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.btn_new);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.tb_s_accountid);
            this.Controls.Add(this.lb_accountid);
            this.Controls.Add(this.dgb_account);
            this.Name = "account";
            this.Text = "アカウント設定";
            ((System.ComponentModel.ISupportInitialize)(this.dgb_account)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgb_account;
        private System.Windows.Forms.Label lb_accountid;
        private System.Windows.Forms.TextBox tb_s_accountid;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Label lb_accountid2;
        private System.Windows.Forms.TextBox tb_accountid;
        private System.Windows.Forms.Label lb_pass;
        private System.Windows.Forms.TextBox tb_pass;
        private System.Windows.Forms.CheckBox cb_kengen1;
        private System.Windows.Forms.CheckBox cb_kengen2;
        private System.Windows.Forms.Button bt_return;
    }
}