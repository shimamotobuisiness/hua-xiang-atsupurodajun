﻿namespace condition_tool
{
    partial class condition_main
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.shinpin_btn1 = new System.Windows.Forms.Button();
            this.setumei1_txtbox = new System.Windows.Forms.TextBox();
            this.copy1_btn = new System.Windows.Forms.Button();
            this.hanei1_btn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.huzokuhin_txtbox = new System.Windows.Forms.TextBox();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ipod_ckbx2 = new System.Windows.Forms.CheckBox();
            this.ipod_ckbx1 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ato_txtbox = new System.Windows.Forms.TextBox();
            this.pr_txtbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ipod_3msg = new System.Windows.Forms.ComboBox();
            this.ipod_2msg = new System.Windows.Forms.ComboBox();
            this.ipod_1msg = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.clear_btn = new System.Windows.Forms.Button();
            this.bt_return = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 36);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1090, 560);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.shinpin_btn1);
            this.tabPage1.Controls.Add(this.setumei1_txtbox);
            this.tabPage1.Controls.Add(this.copy1_btn);
            this.tabPage1.Controls.Add(this.hanei1_btn);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.huzokuhin_txtbox);
            this.tabPage1.Controls.Add(this.button25);
            this.tabPage1.Controls.Add(this.button24);
            this.tabPage1.Controls.Add(this.button23);
            this.tabPage1.Controls.Add(this.button22);
            this.tabPage1.Controls.Add(this.button21);
            this.tabPage1.Controls.Add(this.button20);
            this.tabPage1.Controls.Add(this.button19);
            this.tabPage1.Controls.Add(this.button18);
            this.tabPage1.Controls.Add(this.button17);
            this.tabPage1.Controls.Add(this.button16);
            this.tabPage1.Controls.Add(this.button15);
            this.tabPage1.Controls.Add(this.button14);
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.button11);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.ipod_ckbx2);
            this.tabPage1.Controls.Add(this.ipod_ckbx1);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.ato_txtbox);
            this.tabPage1.Controls.Add(this.pr_txtbox);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.ipod_3msg);
            this.tabPage1.Controls.Add(this.ipod_2msg);
            this.tabPage1.Controls.Add(this.ipod_1msg);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1082, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "iPod/Walkman";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // shinpin_btn1
            // 
            this.shinpin_btn1.Location = new System.Drawing.Point(395, 373);
            this.shinpin_btn1.Name = "shinpin_btn1";
            this.shinpin_btn1.Size = new System.Drawing.Size(226, 23);
            this.shinpin_btn1.TabIndex = 2;
            this.shinpin_btn1.Text = "新品説明文を作成してコピー";
            this.shinpin_btn1.UseVisualStyleBackColor = true;
            this.shinpin_btn1.Click += new System.EventHandler(this.shinpin_btn1_Click);
            // 
            // setumei1_txtbox
            // 
            this.setumei1_txtbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.setumei1_txtbox.Location = new System.Drawing.Point(9, 402);
            this.setumei1_txtbox.Multiline = true;
            this.setumei1_txtbox.Name = "setumei1_txtbox";
            this.setumei1_txtbox.Size = new System.Drawing.Size(1059, 112);
            this.setumei1_txtbox.TabIndex = 43;
            // 
            // copy1_btn
            // 
            this.copy1_btn.Location = new System.Drawing.Point(202, 373);
            this.copy1_btn.Name = "copy1_btn";
            this.copy1_btn.Size = new System.Drawing.Size(187, 23);
            this.copy1_btn.TabIndex = 42;
            this.copy1_btn.Text = "作成した文章をコピー";
            this.copy1_btn.UseVisualStyleBackColor = true;
            this.copy1_btn.Click += new System.EventHandler(this.copy1_btn_Click);
            // 
            // hanei1_btn
            // 
            this.hanei1_btn.Location = new System.Drawing.Point(9, 373);
            this.hanei1_btn.Name = "hanei1_btn";
            this.hanei1_btn.Size = new System.Drawing.Size(187, 23);
            this.hanei1_btn.TabIndex = 41;
            this.hanei1_btn.Text = "上記を反映して文章を作成";
            this.hanei1_btn.UseVisualStyleBackColor = true;
            this.hanei1_btn.Click += new System.EventHandler(this.hanei1_btn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(6, 341);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 15);
            this.label10.TabIndex = 40;
            this.label10.Text = "【完成文】";
            // 
            // huzokuhin_txtbox
            // 
            this.huzokuhin_txtbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.huzokuhin_txtbox.Location = new System.Drawing.Point(9, 276);
            this.huzokuhin_txtbox.Multiline = true;
            this.huzokuhin_txtbox.Name = "huzokuhin_txtbox";
            this.huzokuhin_txtbox.Size = new System.Drawing.Size(1059, 50);
            this.huzokuhin_txtbox.TabIndex = 39;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(884, 235);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(55, 23);
            this.button25.TabIndex = 38;
            this.button25.Text = "×２つ";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(783, 235);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(95, 23);
            this.button24.TabIndex = 37;
            this.button24.Text = "(一部書込あり)";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(702, 235);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 36;
            this.button23.Text = "(書込あり)";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(607, 235);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(89, 23);
            this.button22.TabIndex = 35;
            this.button22.Text = "(一部破れあり)";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(526, 235);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 34;
            this.button21.Text = "(破れあり)";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(430, 235);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(90, 23);
            this.button20.TabIndex = 33;
            this.button20.Text = "(一部汚れあり)";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(349, 235);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 32;
            this.button19.Text = "(汚れあり)";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(257, 235);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(86, 23);
            this.button18.TabIndex = 31;
            this.button18.Text = "(一部ヤケあり)";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(176, 235);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 30;
            this.button17.Text = "(ヤケあり)";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(121, 235);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(49, 23);
            this.button16.TabIndex = 29;
            this.button16.Text = "(  )";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(526, 206);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 28;
            this.button15.Text = "純正元箱";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(445, 206);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 27;
            this.button14.Text = "取扱説明書";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(364, 206);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 26;
            this.button13.Text = "CD-ROM";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(283, 206);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 25;
            this.button12.Text = "ハードケース";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(202, 206);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 24;
            this.button11.Text = "ソフトケース";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(121, 206);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 23;
            this.button10.Text = "アタッチメント";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(562, 177);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 22;
            this.button9.Text = "イヤーピース";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(445, 177);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(111, 23);
            this.button8.TabIndex = 21;
            this.button8.Text = "イヤホン延長コード";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(364, 177);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 20;
            this.button7.Text = "純正イヤホン";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(283, 177);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 19;
            this.button6.Text = "ACアダプタ";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(202, 177);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 18;
            this.button5.Text = "USBケーブル";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(121, 177);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "本体";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(9, 177);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "本体のみ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(467, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 24);
            this.label9.TabIndex = 15;
            this.label9.Text = "いずれかがONの場合は\r\n下欄の入力は不要";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(441, 133);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 37);
            this.label8.TabIndex = 14;
            this.label8.Text = "｝";
            // 
            // ipod_ckbx2
            // 
            this.ipod_ckbx2.AutoSize = true;
            this.ipod_ckbx2.Location = new System.Drawing.Point(9, 155);
            this.ipod_ckbx2.Name = "ipod_ckbx2";
            this.ipod_ckbx2.Size = new System.Drawing.Size(440, 16);
            this.ipod_ckbx2.TabIndex = 13;
            this.ipod_ckbx2.Text = "元箱は無いが、その他全ての付属品が揃っている場合は、このチェックボックスをONにする。";
            this.ipod_ckbx2.UseVisualStyleBackColor = true;
            // 
            // ipod_ckbx1
            // 
            this.ipod_ckbx1.AutoSize = true;
            this.ipod_ckbx1.Location = new System.Drawing.Point(9, 133);
            this.ipod_ckbx1.Name = "ipod_ckbx1";
            this.ipod_ckbx1.Size = new System.Drawing.Size(398, 16);
            this.ipod_ckbx1.TabIndex = 12;
            this.ipod_ckbx1.Text = "元箱含めて、全ての付属品が揃っている場合は、このチェックボックスをONにする。";
            this.ipod_ckbx1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(6, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "【付属品】";
            // 
            // ato_txtbox
            // 
            this.ato_txtbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ato_txtbox.Location = new System.Drawing.Point(788, 27);
            this.ato_txtbox.Multiline = true;
            this.ato_txtbox.Name = "ato_txtbox";
            this.ato_txtbox.Size = new System.Drawing.Size(280, 67);
            this.ato_txtbox.TabIndex = 10;
            // 
            // pr_txtbox
            // 
            this.pr_txtbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pr_txtbox.Location = new System.Drawing.Point(494, 27);
            this.pr_txtbox.Multiline = true;
            this.pr_txtbox.Name = "pr_txtbox";
            this.pr_txtbox.Size = new System.Drawing.Size(280, 67);
            this.pr_txtbox.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(786, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "●後述事項●";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(492, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "●アピールポイント●";
            // 
            // ipod_3msg
            // 
            this.ipod_3msg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ipod_3msg.FormattingEnabled = true;
            this.ipod_3msg.Items.AddRange(new object[] {
            "あああああああ",
            "いいいいいいいい",
            "ううううううう",
            "ええええええええ"});
            this.ipod_3msg.Location = new System.Drawing.Point(53, 74);
            this.ipod_3msg.Name = "ipod_3msg";
            this.ipod_3msg.Size = new System.Drawing.Size(414, 20);
            this.ipod_3msg.TabIndex = 6;
            // 
            // ipod_2msg
            // 
            this.ipod_2msg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ipod_2msg.FormattingEnabled = true;
            this.ipod_2msg.Items.AddRange(new object[] {
            "あああああああ",
            "いいいいいいいい",
            "ううううううう",
            "ええええええええ",
            "おおおおおおおお"});
            this.ipod_2msg.Location = new System.Drawing.Point(53, 51);
            this.ipod_2msg.Name = "ipod_2msg";
            this.ipod_2msg.Size = new System.Drawing.Size(414, 20);
            this.ipod_2msg.TabIndex = 5;
            // 
            // ipod_1msg
            // 
            this.ipod_1msg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ipod_1msg.FormattingEnabled = true;
            this.ipod_1msg.Items.AddRange(new object[] {
            "●あああああああ",
            "●いいいいいいいい",
            "●ううううううう",
            "●ええええええええ"});
            this.ipod_1msg.Location = new System.Drawing.Point(53, 27);
            this.ipod_1msg.Name = "ipod_1msg";
            this.ipod_1msg.Size = new System.Drawing.Size(414, 20);
            this.ipod_1msg.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "３文目";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "２文目";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "１文目";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "【コンディション説明】";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1082, 534);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "カメラ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // clear_btn
            // 
            this.clear_btn.Location = new System.Drawing.Point(12, 7);
            this.clear_btn.Name = "clear_btn";
            this.clear_btn.Size = new System.Drawing.Size(238, 23);
            this.clear_btn.TabIndex = 1;
            this.clear_btn.Text = "入力中のコンディションをクリア";
            this.clear_btn.UseVisualStyleBackColor = true;
            this.clear_btn.Click += new System.EventHandler(this.clear_btn_Click);
            // 
            // bt_return
            // 
            this.bt_return.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_return.Location = new System.Drawing.Point(1020, 12);
            this.bt_return.Name = "bt_return";
            this.bt_return.Size = new System.Drawing.Size(75, 23);
            this.bt_return.TabIndex = 2;
            this.bt_return.Text = "戻る";
            this.bt_return.UseVisualStyleBackColor = true;
            this.bt_return.Click += new System.EventHandler(this.bt_return_Click);
            // 
            // condition_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 608);
            this.Controls.Add(this.bt_return);
            this.Controls.Add(this.clear_btn);
            this.Controls.Add(this.tabControl1);
            this.Name = "condition_main";
            this.Text = "コンディション作成";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button clear_btn;
        private System.Windows.Forms.Button shinpin_btn1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ipod_1msg;
        private System.Windows.Forms.ComboBox ipod_3msg;
        private System.Windows.Forms.ComboBox ipod_2msg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox pr_txtbox;
        private System.Windows.Forms.TextBox ato_txtbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox ipod_ckbx2;
        private System.Windows.Forms.CheckBox ipod_ckbx1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.TextBox huzokuhin_txtbox;
        private System.Windows.Forms.Button copy1_btn;
        private System.Windows.Forms.Button hanei1_btn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox setumei1_txtbox;
        private System.Windows.Forms.Button bt_return;
    }
}

