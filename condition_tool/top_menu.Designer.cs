﻿namespace condition_tool
{
    partial class top_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_condition = new System.Windows.Forms.Button();
            this.btn_amz_img = new System.Windows.Forms.Button();
            this.btn_yahoo_img = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "マスタ設定";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(140, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "アカウント設定";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_condition
            // 
            this.btn_condition.Location = new System.Drawing.Point(27, 78);
            this.btn_condition.Name = "btn_condition";
            this.btn_condition.Size = new System.Drawing.Size(201, 23);
            this.btn_condition.TabIndex = 2;
            this.btn_condition.Text = "コンディション作成";
            this.btn_condition.UseVisualStyleBackColor = true;
            this.btn_condition.Click += new System.EventHandler(this.btn_condition_Click);
            // 
            // btn_amz_img
            // 
            this.btn_amz_img.Location = new System.Drawing.Point(27, 118);
            this.btn_amz_img.Name = "btn_amz_img";
            this.btn_amz_img.Size = new System.Drawing.Size(201, 23);
            this.btn_amz_img.TabIndex = 3;
            this.btn_amz_img.Text = "Amazon画像アップロード";
            this.btn_amz_img.UseVisualStyleBackColor = true;
            this.btn_amz_img.Click += new System.EventHandler(this.btn_amz_img_Click);
            // 
            // btn_yahoo_img
            // 
            this.btn_yahoo_img.Location = new System.Drawing.Point(27, 147);
            this.btn_yahoo_img.Name = "btn_yahoo_img";
            this.btn_yahoo_img.Size = new System.Drawing.Size(201, 23);
            this.btn_yahoo_img.TabIndex = 4;
            this.btn_yahoo_img.Text = "ヤフオク画像アップロード";
            this.btn_yahoo_img.UseVisualStyleBackColor = true;
            this.btn_yahoo_img.Click += new System.EventHandler(this.btn_yahoo_img_Click);
            // 
            // top_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_yahoo_img);
            this.Controls.Add(this.btn_amz_img);
            this.Controls.Add(this.btn_condition);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "top_menu";
            this.Text = "top_menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.top_menu_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btn_condition;
        private System.Windows.Forms.Button btn_amz_img;
        private System.Windows.Forms.Button btn_yahoo_img;
    }
}