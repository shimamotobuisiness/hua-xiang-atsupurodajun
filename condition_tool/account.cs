﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Security.Permissions;
using System.Text.RegularExpressions;

namespace condition_tool
{
    public partial class account : Form
    {
        public object DataGridView1 { get; private set; }

        static Boolean cb_kengen1_status;
        static Boolean cb_kengen2_status;

        public account()
        {
            InitializeComponent();

            cb_kengen1_status = false;
            cb_kengen2_status = false;

            //ヘッダー初期表示
            DataTable search_result = new DataTable();

            search_result.Columns.Add("account_id", typeof(string));
            search_result.Columns.Add("condition_auth", typeof(string));
            search_result.Columns.Add("account_auth", typeof(string));
            search_result.Columns.Add("ins_dtime", typeof(string));
            search_result.Columns.Add("ins_user", typeof(string));
            search_result.Columns.Add("upd_dtime", typeof(string));
            search_result.Columns.Add("upd_user", typeof(string));
            search_result.Columns.Add("account_password", typeof(string));

            dgb_account.DataSource = search_result;

            //列のテキストを変更する
            dgb_account.Columns[0].HeaderText = "アカウントID";
            dgb_account.Columns[1].HeaderText = "コンディション設定マスタ権限";
            dgb_account.Columns[2].HeaderText = "アカウントマスタ権限";
            dgb_account.Columns[3].HeaderText = "登録日時";
            dgb_account.Columns[4].HeaderText = "登録アカウントID";
            dgb_account.Columns[5].HeaderText = "変更日時";
            dgb_account.Columns[6].HeaderText = "変更アカウントID";
            dgb_account.Columns[7].HeaderText = "パスワード";
            this.dgb_account.Columns[7].Visible = false;

            // 選択されているセルをなくす
            dgb_account.CurrentCell = null;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            try
            {
                string db_file = "blue_needs.db";

                using (var conn = new SQLiteConnection("Data Source=" + db_file))
                {
                    conn.Open();

                    string sql = " select a.account_id,a.account_password,a.condition_auth,a.account_auth,a.ins_dtime,a.ins_user,a.upd_dtime,a.upd_user  from account a ";
                    sql += " where a.del_flg = 0 ";
                    sql += " order by a.account_id ";

                    using (SQLiteCommand command = conn.CreateCommand())
                    {
                        command.CommandText = sql;



                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            DataTable search_result = new DataTable();

                            search_result.Columns.Add("account_id", typeof(string));
                            search_result.Columns.Add("condition_auth", typeof(string));
                            search_result.Columns.Add("account_auth", typeof(string));
                            search_result.Columns.Add("ins_dtime", typeof(string));
                            search_result.Columns.Add("ins_user", typeof(string));
                            search_result.Columns.Add("upd_dtime", typeof(string));
                            search_result.Columns.Add("upd_user", typeof(string));
                            search_result.Columns.Add("account_password", typeof(string));

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //新しい行作成
                                    DataRow row = search_result.NewRow();

                                    //新しい行の各列にセット
                                    row["account_id"] = reader.GetValue(0);

                                    if (reader.GetValue(2).ToString() == "1")
                                    {
                                        row["condition_auth"] = "権限あり";
                                    }
                                    else
                                    {
                                        row["condition_auth"] = "権限なし";
                                    }

                                    if (reader.GetValue(3).ToString() == "1")
                                    {
                                        row["account_auth"] = "権限あり";
                                    }
                                    else
                                    {
                                        row["account_auth"] = "権限なし";
                                    }

                                    row["ins_dtime"] = reader.GetValue(4);
                                    row["ins_user"] = reader.GetValue(5);
                                    row["upd_dtime"] = reader.GetValue(6);
                                    row["upd_user"] = reader.GetValue(7);
                                    row["account_password"] = reader.GetValue(1);

                                    search_result.Rows.Add(row);
                                }

                                dgb_account.DataSource = search_result;


                            }

                            reader.Close();
                            // 選択されているセルをなくす
                            dgb_account.CurrentCell = null;
                        }

                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            try
            {
                this.tb_accountid.Text = "";  //ID表示
                this.tb_pass.Text = "";  //パスワード表示
                cb_kengen1.Checked = false;       //コンディション設定マスタ権限
                cb_kengen2.Checked = false;       //アカウント設定マスタ権限

                cb_kengen1_status = false;
                cb_kengen2_status = false;

                // 選択されているセルをなくす
                dgb_account.CurrentCell = null;

                // textBox1 を読み取り専用を解除
                this.tb_accountid.ReadOnly = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                //チェック
                if (this.tb_accountid.Text == "")
                {
                    MessageBox.Show("アカウントIDが入力されていません。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    return;
                }
                if (this.tb_pass.Text == "")
                {
                    MessageBox.Show("パスワードが入力されていません。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    return;
                }

                // 半角英数 チェック
                if (!(Regex.Match(this.tb_accountid.Text, "^[a-zA-Z0-9]+$")).Success)
                {
                    // 半角英数以外の文字が含まれています。
                    MessageBox.Show("アカウントIDに半角英数以外の文字が含まれています。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    return;
                }
                if (!(Regex.Match(this.tb_pass.Text, "^[a-zA-Z0-9]+$")).Success)
                {
                    // 半角英数以外の文字が含まれています。
                    MessageBox.Show("パスワードに半角英数以外の文字が含まれています。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    return;
                }

                int gyou_now = 0;
                int cb_kengen1_now = 0;
                int cb_kengen2_now = 0;

                //現在のセルの行インデックスをがない場合は終了
                if (dgb_account.CurrentCell == null)
                {
                    return;
                }
                gyou_now = dgb_account.CurrentCell.RowIndex;
                //現在のセルの値を表示
                if (dgb_account.Rows[gyou_now].Cells[1].Value.ToString() == "権限あり")
                {
                    cb_kengen1_now = 1;       //コンディション設定マスタ権限
                }
                else
                {
                    cb_kengen1_now = 0;       //コンディション設定マスタ権限
                }

                if (dgb_account.Rows[gyou_now].Cells[2].Value.ToString() == "権限あり")
                {
                    cb_kengen2_now = 1;       //アカウント設定マスタ権限
                }
                else
                {
                    cb_kengen2_now = 0;       //アカウント設定マスタ権限
                }

                //変更があれば登録
                if (this.tb_accountid.Modified == true || this.tb_pass.Modified == true || cb_kengen1_status == true || cb_kengen2_status == true)
                {

                    //アカウントIDがすでに存在するかチェック
                    string db_file = "blue_needs.db";
                    DataTable search_result = new DataTable();

                    using (var conn = new SQLiteConnection("Data Source=" + db_file))
                    {
                        conn.Open();

                        string sql = " select count(a.account_id) as cnt_account from account a ";
                        sql += " where a.account_id = @p_account_id ";

                        SQLiteParameter[] parameters = new SQLiteParameter[]{
                                new SQLiteParameter("@p_account_id",this.tb_accountid.Text.ToString())
                            };


                        using (SQLiteCommand command = conn.CreateCommand())
                        {
                            command.Parameters.Add(parameters[0]);
                            command.CommandText = sql;



                            using (SQLiteDataReader reader = command.ExecuteReader())
                            {
                                

                                search_result.Columns.Add("cnt_account", typeof(int));


                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        //新しい行作成
                                        DataRow row = search_result.NewRow();

                                        //新しい行の各列にセット
                                        row["cnt_account"] = reader.GetValue(0);


                                        search_result.Rows.Add(row);
                                    }
                                }

                                reader.Close();

                            }
                        }
                    }

                    if (int.Parse(search_result.Rows[0][0].ToString()) > 0) {

                        DialogResult result = MessageBox.Show("更新を実施してよろしいでしょうか？" ,

                                                    "確認",
                                                    MessageBoxButtons.OKCancel,
                                                    MessageBoxIcon.Warning);
                        if (result == DialogResult.OK)
                        {

                            //変更を更新
                            DataTable upd_result = new DataTable();

                            using (var conn = new SQLiteConnection("Data Source=" + db_file))
                            {
                                conn.Open();

                                string sql = " update account ";
                                sql += "  set ";
                                sql += "  account_id = @p_aacount_id ";
                                sql += " ,account_password = @p_account_password ";
                                sql += " ,account_auth = @p_account_auth ";
                                sql += " ,condition_auth = @p_condition_auth ";
                                sql += " ,del_flg = @p_del_flg ";
                                sql += " ,upd_dtime = DATETIME('now','localtime') ";
                                sql += " ,upd_user = 'shimamoto' ";
                                sql += " where account_id = @p_aacount_id ";

                                SQLiteParameter[] parameters = new SQLiteParameter[]{
                                new SQLiteParameter("@p_account_id",this.tb_accountid.Text.ToString()),
                                new SQLiteParameter("@p_account_password",this.tb_pass.Text.ToString()),
                                new SQLiteParameter("@p_account_auth",cb_kengen1_now),
                                new SQLiteParameter("@p_condition_auth",cb_kengen2_now),
                                //new SQLiteParameter("@p_del_flg",this.tb_accountid.Text.ToString()),
                                //new SQLiteParameter("@p_upd_user",this.tb_accountid.Text.ToString())
                                };


                                using (SQLiteCommand command = conn.CreateCommand())
                                {
                                    command.Parameters.Add(parameters[0]);
                                    command.CommandText = sql;



                                    using (SQLiteDataReader reader = command.ExecuteReader())
                                    {


                                        search_result.Columns.Add("cnt_account", typeof(int));


                                        if (reader.HasRows)
                                        {
                                            while (reader.Read())
                                            {
                                                //新しい行作成
                                                DataRow row = search_result.NewRow();

                                                //新しい行の各列にセット
                                                row["cnt_account"] = reader.GetValue(0);


                                                search_result.Rows.Add(row);
                                            }
                                        }

                                        reader.Close();

                                    }
                                }
                            }

                            this.tb_pass.Modified = false;
                            cb_kengen1_status = false;
                            cb_kengen2_status = false;

                        }
                        else if (result == DialogResult.Cancel)
                        {
                            return;
                        }
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("新規登録を実施してよろしいでしょうか？",

                                                    "確認",
                                                    MessageBoxButtons.OKCancel,
                                                    MessageBoxIcon.Warning);
                        if (result == DialogResult.OK)
                        {

                            this.tb_pass.Modified = false;

                        }
                        else if (result == DialogResult.Cancel)
                        {
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("変更がありません。",
                                               "エラー",
                                               MessageBoxButtons.OK,
                                               MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }
        }

        private void bt_return_Click(object sender, EventArgs e)
        {
            top_menu frm = new top_menu();
            this.Visible = false;
            frm.Show();
        }


        private void tb_accountid_KeyPress(object sender, KeyPressEventArgs e)
        {
            //0～9と、バックスペース以外の時は、イベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && (e.KeyChar < 'a' || 'z' < e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void tb_s_accountid_KeyPress(object sender, KeyPressEventArgs e)
        {
            //0～9と、バックスペース以外の時は、イベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && (e.KeyChar < 'a' || 'z' < e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void tb_pass_KeyPress(object sender, KeyPressEventArgs e)
        {
            //0～9と、バックスペース以外の時は、イベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && (e.KeyChar < 'a' || 'z' < e.KeyChar) && (e.KeyChar < 'A' || 'Z' < e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;

            }
        }

        private void dgb_account_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int gyou_now = 0;


                //現在のセルの行インデックスをがない場合は終了
                if (dgb_account.CurrentCell == null)
                {
                    return;
                }
                gyou_now = dgb_account.CurrentCell.RowIndex;
                //現在のセルの値を表示
                this.tb_accountid.Text = dgb_account.Rows[gyou_now].Cells[0].Value.ToString();  //ID表示
                this.tb_pass.Text = dgb_account.Rows[gyou_now].Cells[7].Value.ToString();  //パスワード表示

                if (dgb_account.Rows[gyou_now].Cells[1].Value.ToString() == "権限あり")
                {
                    cb_kengen1.Checked = true;       //コンディション設定マスタ権限
                }
                else
                {
                    cb_kengen1.Checked = false;       //コンディション設定マスタ権限
                }

                if (dgb_account.Rows[gyou_now].Cells[2].Value.ToString() == "権限あり")
                {
                    cb_kengen2.Checked = true;       //アカウント設定マスタ権限
                }
                else
                {
                    cb_kengen2.Checked = false;       //アカウント設定マスタ権限
                }

                // textBox1 を読み取り専用にする
                this.tb_accountid.ReadOnly = true;
                cb_kengen1_status = false;
                cb_kengen2_status = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }
        }

        private void cb_kengen1_CheckedChanged(object sender, EventArgs e)
        {
            cb_kengen1_status = true;
        }

        private void cb_kengen2_CheckedChanged(object sender, EventArgs e)
        {
            cb_kengen2_status = true;
        }
    }

}



