﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace condition_tool
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void bt_login_Click(object sender, EventArgs e)
        {
            try {
                string db_file = "blue_needs.db";
                string ac_pw = "";

                if(tb_userid.Text == "" || tb_password.Text == "")
                {
                    MessageBox.Show("ユーザーIDまたは、パスワードが入力されていません。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                else { 

                    using (var conn = new SQLiteConnection("Data Source=" + db_file))
                     {
                       conn.Open();

                       string sql = " select account_password from account t";
                       sql += " where t.account_id = \"" + tb_userid.Text + "\"";

                        using (SQLiteCommand command = conn.CreateCommand())
                        {
                            command.CommandText = sql;
                            using (SQLiteDataReader reader = command.ExecuteReader())
                            {
                              while (reader.Read())
                                 {
                                    ac_pw = reader["account_password"].ToString();

                                }
                            }

                        }
                        conn.Close();
                    }

                    if (ac_pw == tb_password.Text)
                      {
                         top_menu frm = new top_menu();
                            frm.Show();
                         this.Visible = false;


                    }
                    else {
                              MessageBox.Show("ユーザーIDまたは、パスワードに誤りがあります。",
                                               "エラー",
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Error);
                    }
                }
            }
            catch(Exception ex) {
                MessageBox.Show("Error:" + ex.Message);
                
            }

        }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            //Application.Exit();
        }

        private void login_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose();
            this.Close();
            Application.Exit();
        }

        private void login_Load(object sender, EventArgs e)
        {
            tb_userid.Text = "1";
            tb_password.Text = "1";
        }
    }


}
