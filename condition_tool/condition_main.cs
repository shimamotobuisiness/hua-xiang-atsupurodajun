﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace condition_tool
{
    public partial class condition_main : Form
    {
        string button3_msg;
        string button4_msg;
        string button5_msg;
        string button6_msg;
        string button7_msg;
        string button8_msg;
        string button9_msg;
        string button10_msg;
        string button11_msg;
        string button12_msg;
        string button13_msg;
        string button14_msg;
        string button15_msg;
        string button16_msg;
        string button17_msg;
        string button18_msg;
        string button19_msg;
        string button20_msg;
        string button21_msg;
        string button22_msg;
        string button23_msg;
        string button24_msg;
        string button25_msg;


        public condition_main()
        {
            InitializeComponent();
            getList1();
            getList2();
            getList3();
            getBtn1();
        }

        private void getList1()
        {
            try{
                string db_file = "blue_needs.db";
                
                using (var conn = new SQLiteConnection("Data Source=" + db_file))
                {
                    conn.Open();

                    string sql =  " select fl1.list_no,fl1.list_nm,fl1.disp_seq from m_frm1_list1 fl1 ";
                           sql += " where fl1.list_kbn = 1 ";
                           sql += " order by fl1.disp_seq ";
                    
                    using (SQLiteCommand command = conn.CreateCommand())
                    {
                        command.CommandText = sql;

                        

                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            DataTable CMB1_SOURCE = new DataTable();
                            CMB1_SOURCE.Columns.Add("list_no", typeof(int));
                            CMB1_SOURCE.Columns.Add("list_nm", typeof(string));
                            CMB1_SOURCE.Columns.Add("disp_seq", typeof(int));

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //新しい行作成
                                    DataRow row = CMB1_SOURCE.NewRow();

                                    //新しい行の各列にセット
                                    row["list_no"] = reader.GetValue(0);
                                    row["list_nm"] = reader.GetValue(1).ToString();
                                    row["disp_seq"] = reader.GetValue(2);

                                    CMB1_SOURCE.Rows.Add(row);
                                }

                                ipod_1msg.DataSource = CMB1_SOURCE;
                                ipod_1msg.DisplayMember = "list_nm";
                                ipod_1msg.ValueMember = "disp_seq";

                            }

                            reader.Close();

                        }

                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }

        }

        private void getList2()
        {
            try
            {
                string db_file = "blue_needs.db";

                using (var conn = new SQLiteConnection("Data Source=" + db_file))
                {
                    conn.Open();

                    string sql = " select fl1.list_no,fl1.list_nm,fl1.disp_seq from m_frm1_list1 fl1 ";
                    sql += " where fl1.list_kbn = 2 ";
                    sql += " order by fl1.disp_seq ";

                    using (SQLiteCommand command = conn.CreateCommand())
                    {
                        command.CommandText = sql;



                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            DataTable CMB1_SOURCE = new DataTable();
                            CMB1_SOURCE.Columns.Add("list_no", typeof(int));
                            CMB1_SOURCE.Columns.Add("list_nm", typeof(string));
                            CMB1_SOURCE.Columns.Add("disp_seq", typeof(int));

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //新しい行作成
                                    DataRow row = CMB1_SOURCE.NewRow();

                                    //新しい行の各列にセット
                                    row["list_no"] = reader.GetValue(0);
                                    row["list_nm"] = reader.GetValue(1).ToString();
                                    row["disp_seq"] = reader.GetValue(2);

                                    CMB1_SOURCE.Rows.Add(row);
                                }

                                ipod_2msg.DataSource = CMB1_SOURCE;
                                ipod_2msg.DisplayMember = "list_nm";
                                ipod_2msg.ValueMember = "disp_seq";

                            }

                            reader.Close();

                        }

                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }

        }

        private void getList3()
        {
            try
            {
                string db_file = "blue_needs.db";

                using (var conn = new SQLiteConnection("Data Source=" + db_file))
                {
                    conn.Open();

                    string sql = " select fl1.list_no,fl1.list_nm,fl1.disp_seq from m_frm1_list1 fl1 ";
                    sql += " where fl1.list_kbn = 3 ";
                    sql += " order by fl1.disp_seq ";

                    using (SQLiteCommand command = conn.CreateCommand())
                    {
                        command.CommandText = sql;



                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            DataTable CMB1_SOURCE = new DataTable();
                            CMB1_SOURCE.Columns.Add("list_no", typeof(int));
                            CMB1_SOURCE.Columns.Add("list_nm", typeof(string));
                            CMB1_SOURCE.Columns.Add("disp_seq", typeof(int));

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //新しい行作成
                                    DataRow row = CMB1_SOURCE.NewRow();

                                    //新しい行の各列にセット
                                    row["list_no"] = reader.GetValue(0);
                                    row["list_nm"] = reader.GetValue(1).ToString();
                                    row["disp_seq"] = reader.GetValue(2);

                                    CMB1_SOURCE.Rows.Add(row);
                                }

                                ipod_3msg.DataSource = CMB1_SOURCE;
                                ipod_3msg.DisplayMember = "list_nm";
                                ipod_3msg.ValueMember = "disp_seq";

                            }

                            reader.Close();

                        }

                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }

        }

        private void getBtn1()
        {
            try
            {
                string db_file = "blue_needs.db";

                using (var conn = new SQLiteConnection("Data Source=" + db_file))
                {
                    conn.Open();

                    string sql = " select fl1.btn_no,fl1.btn_nm,fl1.btn_msg from m_frm1_btn1 fl1  ";
                    sql += " where fl1.btn_kbn = 1 ";
                    sql += " order by fl1.disp_seq ";

                    using (SQLiteCommand command = conn.CreateCommand())
                    {
                        command.CommandText = sql;



                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            DataTable BTN1_SOURCE = new DataTable();
                            BTN1_SOURCE.Columns.Add("btn_no", typeof(int));
                            BTN1_SOURCE.Columns.Add("btn_nm", typeof(string));
                            BTN1_SOURCE.Columns.Add("btn_msg", typeof(string));

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    //新しい行作成
                                    DataRow row = BTN1_SOURCE.NewRow();

                                    //新しい行の各列にセット
                                    row["btn_no"] = reader.GetValue(0);
                                    row["btn_nm"] = reader.GetValue(1).ToString();
                                    row["btn_msg"] = reader.GetValue(2).ToString();

                                    BTN1_SOURCE.Rows.Add(row);
                                }

                                button3.Text = BTN1_SOURCE.Rows[0][1].ToString();
                                button4.Text = BTN1_SOURCE.Rows[1][1].ToString();
                                button5.Text = BTN1_SOURCE.Rows[2][1].ToString();
                                button6.Text = BTN1_SOURCE.Rows[3][1].ToString();
                                button7.Text = BTN1_SOURCE.Rows[4][1].ToString();
                                button8.Text = BTN1_SOURCE.Rows[5][1].ToString();
                                button9.Text = BTN1_SOURCE.Rows[6][1].ToString();
                                button10.Text = BTN1_SOURCE.Rows[7][1].ToString();
                                button11.Text = BTN1_SOURCE.Rows[8][1].ToString();
                                button12.Text = BTN1_SOURCE.Rows[9][1].ToString();
                                button13.Text = BTN1_SOURCE.Rows[10][1].ToString();
                                button14.Text = BTN1_SOURCE.Rows[11][1].ToString();
                                button15.Text = BTN1_SOURCE.Rows[12][1].ToString();
                                button16.Text = BTN1_SOURCE.Rows[13][1].ToString();
                                button17.Text = BTN1_SOURCE.Rows[14][1].ToString();
                                button18.Text = BTN1_SOURCE.Rows[15][1].ToString();
                                button19.Text = BTN1_SOURCE.Rows[16][1].ToString();
                                button20.Text = BTN1_SOURCE.Rows[17][1].ToString();
                                button21.Text = BTN1_SOURCE.Rows[18][1].ToString();
                                button22.Text = BTN1_SOURCE.Rows[19][1].ToString();
                                button23.Text = BTN1_SOURCE.Rows[20][1].ToString();
                                button24.Text = BTN1_SOURCE.Rows[21][1].ToString();
                                button25.Text = BTN1_SOURCE.Rows[22][1].ToString();

                                button3_msg = BTN1_SOURCE.Rows[0][2].ToString();
                                button4_msg = BTN1_SOURCE.Rows[1][2].ToString();
                                button5_msg = BTN1_SOURCE.Rows[2][2].ToString();
                                button6_msg = BTN1_SOURCE.Rows[3][2].ToString();
                                button7_msg = BTN1_SOURCE.Rows[4][2].ToString();
                                button8_msg = BTN1_SOURCE.Rows[5][2].ToString();
                                button9_msg = BTN1_SOURCE.Rows[6][2].ToString();
                                button10_msg = BTN1_SOURCE.Rows[7][2].ToString();
                                button11_msg = BTN1_SOURCE.Rows[8][2].ToString();
                                button12_msg = BTN1_SOURCE.Rows[9][2].ToString();
                                button13_msg = BTN1_SOURCE.Rows[10][2].ToString();
                                button14_msg = BTN1_SOURCE.Rows[11][2].ToString();
                                button15_msg = BTN1_SOURCE.Rows[12][2].ToString();
                                button16_msg = BTN1_SOURCE.Rows[13][2].ToString();
                                button17_msg = BTN1_SOURCE.Rows[14][2].ToString();
                                button18_msg = BTN1_SOURCE.Rows[15][2].ToString();
                                button19_msg = BTN1_SOURCE.Rows[16][2].ToString();
                                button20_msg = BTN1_SOURCE.Rows[17][2].ToString();
                                button21_msg = BTN1_SOURCE.Rows[18][2].ToString();
                                button22_msg = BTN1_SOURCE.Rows[19][2].ToString();
                                button23_msg = BTN1_SOURCE.Rows[20][2].ToString();
                                button24_msg = BTN1_SOURCE.Rows[21][2].ToString();
                                button25_msg = BTN1_SOURCE.Rows[22][2].ToString();




                            }

                            reader.Close();

                        }

                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
                this.Close();
            }

        }


        private void button3_Click(object sender, EventArgs e)
        {
            this.huzokuhin_txtbox.ResetText();
            //this.huzokuhin_txtbox.AppendText("本体のほかに付属品はございません。");
            this.huzokuhin_txtbox.AppendText(button3_msg);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("本体");
            this.huzokuhin_txtbox.AppendText(button4_msg);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、USBケーブル");
            this.huzokuhin_txtbox.AppendText(button5_msg);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、ACアダプタ");
            this.huzokuhin_txtbox.AppendText(button6_msg);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、純正イヤホン");
            this.huzokuhin_txtbox.AppendText(button7_msg);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、イヤホン延長コード");
            this.huzokuhin_txtbox.AppendText(button8_msg);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、イヤーピース");
            this.huzokuhin_txtbox.AppendText(button9_msg);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、アタッチメント");
            this.huzokuhin_txtbox.AppendText(button10_msg);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、ソフトケース");
            this.huzokuhin_txtbox.AppendText(button11_msg);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、ハードケース");
            this.huzokuhin_txtbox.AppendText(button12_msg);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、CD-ROM");
            this.huzokuhin_txtbox.AppendText(button13_msg);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、取扱説明書");
            this.huzokuhin_txtbox.AppendText(button14_msg);

        }

        private void button15_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("、純正元箱");
            this.huzokuhin_txtbox.AppendText(button15_msg);

        }

        private void button16_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("()");
            this.huzokuhin_txtbox.AppendText(button16_msg);

        }

        private void button17_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(ヤケあり)");
            this.huzokuhin_txtbox.AppendText(button17_msg);

        }

        private void button18_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(一部ヤケあり)");
            this.huzokuhin_txtbox.AppendText(button18_msg);

        }

        private void button19_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(汚れあり)");
            this.huzokuhin_txtbox.AppendText(button19_msg);

        }

        private void button20_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(一部汚れあり)");
            this.huzokuhin_txtbox.AppendText(button20_msg);

        }

        private void button21_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(破れあり)");
            this.huzokuhin_txtbox.AppendText(button21_msg);

        }

        private void button22_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(一部破れあり)");
            this.huzokuhin_txtbox.AppendText(button22_msg);

        }

        private void button23_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(書込あり)");
            this.huzokuhin_txtbox.AppendText(button23_msg);

        }

        private void button24_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("(一部書込あり)");
            this.huzokuhin_txtbox.AppendText(button24_msg);

        }

        private void button25_Click(object sender, EventArgs e)
        {
            //this.huzokuhin_txtbox.AppendText("×２つ");
            this.huzokuhin_txtbox.AppendText(button25_msg);

        }

        private void clear_btn_Click(object sender, EventArgs e)
        {
            this.huzokuhin_txtbox.ResetText();
            this.pr_txtbox.ResetText();
            this.ato_txtbox.ResetText();
        }

        //上記を反映して文章を作成
        private void hanei1_btn_Click(object sender, EventArgs e)
        {
            this.setumei1_txtbox.ResetText();

            if (this.ipod_ckbx2.Checked == true)
            {
                this.setumei1_txtbox.AppendText("●必ずご一読ください●付属品に不足無く、全て揃っております。※外箱のみございませんので予めご了承ください。●●Amazon.co.jpが保管・梱包、即日発送（365日24時間対応）を行いますのでご安心ください。お支払・配送にはAmazonの各種サービス（お急ぎ便やギフトラッピング、コンビ二決済、ネットバンクなど）を利用頂けます。●万が一商品に不備がございましたら、Amazon規約に基づいて返品にも対応しています。●元箱がある場合は付属品欄に記載をしております。また、中古品の場合、元箱に傷みがある場合がございますので、予めご了承くださいませ。●中古品のため「全ての付属品」に取扱説明書を除く書類（保証書や案内用紙）は含まれない場合がございます。●悪質な返品（中身すり替え、付属品盗難、虚偽の申告、お客様ご自身での破損による返品等）に対しては、Amazonへの購入者アカウント調査申請、及び然るべき法的処置を取らせていただきます。●気持ちの良いお取引を心掛けております。どうぞよろしくお願いいたします。");
            }
            if (this.ipod_ckbx1.Checked == true) {
                this.setumei1_txtbox.ResetText();
                this.setumei1_txtbox.AppendText("●必ずご一読ください●元箱含め全ての付属品が揃っております。●●Amazon.co.jpが保管・梱包、即日発送（365日24時間対応）を行いますのでご安心ください。お支払・配送にはAmazonの各種サービス（お急ぎ便やギフトラッピング、コンビ二決済、ネットバンクなど）を利用頂けます。●万が一商品に不備がございましたら、Amazon規約に基づいて返品にも対応しています。●元箱がある場合は付属品欄に記載をしております。また、中古品の場合、元箱に傷みがある場合がございますので、予めご了承くださいませ。●中古品のため「全ての付属品」に取扱説明書を除く書類（保証書や案内用紙）は含まれない場合がございます。●悪質な返品（中身すり替え、付属品盗難、虚偽の申告、お客様ご自身での破損による返品等）に対しては、Amazonへの購入者アカウント調査申請、及び然るべき法的処置を取らせていただきます。●気持ちの良いお取引を心掛けております。どうぞよろしくお願いいたします。");
            }
            if (this.ipod_ckbx2.Checked == false && this.ipod_ckbx1.Checked == false)
            {
                if (huzokuhin_txtbox.Text.IndexOf("本体のほかに付属品はございません。") >= 0)
                {
                    this.setumei1_txtbox.AppendText("●必ずご一読ください●" + this.pr_txtbox.Text + this.ipod_1msg.Text + this.ipod_2msg.Text + this.ipod_3msg.Text + "●付属品：" + this.huzokuhin_txtbox.Text + "●" + this.ato_txtbox.Text + "●Amazon.co.jpが保管・梱包、即日発送（365日24時間対応）を行いますのでご安心ください。お支払・配送にはAmazonの各種サービス（お急ぎ便やギフトラッピング、コンビ二決済、ネットバンクなど）を利用頂けます。●万が一商品に不備がございましたら、Amazon規約に基づいて返品にも対応しています。●元箱がある場合は付属品欄に記載をしております。また、中古品の場合、元箱に傷みがある場合がございますので、予めご了承くださいませ。●中古品のため「全ての付属品」に取扱説明書を除く書類（保証書や案内用紙）は含まれない場合がございます。●悪質な返品（中身すり替え、付属品盗難、虚偽の申告、お客様ご自身での破損による返品等）に対しては、Amazonへの購入者アカウント調査申請、及び然るべき法的処置を取らせていただきます。●気持ちの良いお取引を心掛けております。どうぞよろしくお願いいたします。");
                }
                else
                {
                    this.setumei1_txtbox.AppendText("●必ずご一読ください●" + this.pr_txtbox.Text + this.ipod_1msg.Text + this.ipod_2msg.Text + this.ipod_3msg.Text + "●付属品：" + this.huzokuhin_txtbox.Text + "。※記載のないものは付属いたしません。" + "●" + this.ato_txtbox.Text + "●Amazon.co.jpが保管・梱包、即日発送（365日24時間対応）を行いますのでご安心ください。お支払・配送にはAmazonの各種サービス（お急ぎ便やギフトラッピング、コンビ二決済、ネットバンクなど）を利用頂けます。●万が一商品に不備がございましたら、Amazon規約に基づいて返品にも対応しています。●元箱がある場合は付属品欄に記載をしております。また、中古品の場合、元箱に傷みがある場合がございますので、予めご了承くださいませ。●中古品のため「全ての付属品」に取扱説明書を除く書類（保証書や案内用紙）は含まれない場合がございます。●悪質な返品（中身すり替え、付属品盗難、虚偽の申告、お客様ご自身での破損による返品等）に対しては、Amazonへの購入者アカウント調査申請、及び然るべき法的処置を取らせていただきます。●気持ちの良いお取引を心掛けております。どうぞよろしくお願いいたします。");
                }
            }

        }
        

        private void copy1_btn_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.setumei1_txtbox.Text);
        }

        private void shinpin_btn1_Click(object sender, EventArgs e)
        {
            this.setumei1_txtbox.ResetText();
            this.setumei1_txtbox.AppendText("●新品未使用品です。●30日間返金保証付き●Amazon.co.jpが保管・梱包、即日発送（365日24時間対応）を行います。お支払・配送にはAmazonの各種サービス（お急ぎ便やギフトラッピング、代金引換、コンビ二決済、ネットバンクなど）を利用頂けます。●万が一商品に不備がございましたら、Amazon規約に基づいて返品にも対応しています。●悪質な返品（中身すり替え、付属品盗難、虚偽の申告、お客様ご自身での破損による返品等）に対しては、Amazonへの購入者アカウント調査申請、及び然るべき法的処置を取らせていただきます。●気持ちの良いお取引を心掛けております。どうぞよろしくお願いいたします。");
            Clipboard.SetText(this.setumei1_txtbox.Text);
        }

        private void bt_return_Click(object sender, EventArgs e)
        {
            top_menu frm = new top_menu();
            this.Visible = false;
            frm.Show();
        }
    }
}
